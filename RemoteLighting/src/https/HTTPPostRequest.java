package https;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.advanchiplighting.remotelighting.GlobalConstants;
import com.advanchiplighting.remotelighting.R;

public class HTTPPostRequest {
	static Context context;
	static JSONObject json;

	public HTTPPostRequest(Context context, JSONObject json) {

		this.json = json;
		this.context = context;

	}

	public static JSONObject runHTTPPost() {

		JSONObject responceJson = new JSONObject();
		String instruction;

		if (json == null) {
			try {
				responceJson.put("Success", false);
				responceJson.put("Description", "invalid input");
			} catch (JSONException e) {
				
				e.printStackTrace();
				return null;
			}

			return responceJson;
		} else {
			try {
				instruction = (String) json.get("instruction");
			} catch (JSONException e) {

				e.printStackTrace();
				return null;
			}
		}

		if (instruction.equals("bindSwitch")) {
			String serialNumber;
			String gatewayID;
			String switchName;
			try {
				serialNumber = (String) json.get("serialNumber");
				gatewayID = (String) json.get("gatewayID");
				switchName=(String)json.get("name");
			} catch (JSONException e3) {
				e3.printStackTrace();
				return null;
			}

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			nameValuePairs.add(new BasicNameValuePair("serial_number",
					serialNumber));
			nameValuePairs.add(new BasicNameValuePair("name",
					switchName));

			SharedPreferences sharedPrefs = PreferenceManager
					.getDefaultSharedPreferences(context
							.getApplicationContext());

			HttpPost request = new HttpPost(GlobalConstants.ADVANCHIP_SERVER
					+ "/" + gatewayID + "/gates");
			String access_token = sharedPrefs.getString("access_token",
					"default");

			request.setHeader("Authorization", "Bearer " + access_token);
			try {
				request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e1) {
				Log.e("http post error",
						"some error occured with the http post request");
				e1.printStackTrace();
				return null;
			}

			// receiving the response
			HttpResponse response = null;
			HttpParams httpParams = new BasicHttpParams();

			KeyStore localTrustStore = null;
			try {
				localTrustStore = KeyStore.getInstance("BKS");
			} catch (KeyStoreException e1) {
				e1.printStackTrace();
				return null;
			}
			InputStream in = context.getResources().openRawResource(
					R.raw.advanchip_trust_store);
			try {
				localTrustStore.load(in, "".toCharArray());
			} catch (NoSuchAlgorithmException e2) {
				e2.printStackTrace();
				return null;
			} catch (CertificateException e2) {
				e2.printStackTrace();
				return null;
			} catch (IOException e2) {
				e2.printStackTrace();
				return null;
			}
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			SSLSocketFactory sslSocketFactory = null;
			try {
				sslSocketFactory = new SSLSocketFactory(localTrustStore);
			} catch (KeyManagementException e1) {

				e1.printStackTrace();
				return null;
			} catch (UnrecoverableKeyException e1) {
				e1.printStackTrace();
				return null;
			} catch (NoSuchAlgorithmException e1) {
				e1.printStackTrace();
				return null;
			} catch (KeyStoreException e1) {
				e1.printStackTrace();
				return null;
			}
			schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));

			ClientConnectionManager cm = new ThreadSafeClientConnManager(
					httpParams, schemeRegistry);

			HttpClient client = new DefaultHttpClient(cm, httpParams);

			try {
				response = client.execute(request);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String output = null;
			try {
				output = EntityUtils.toString(response.getEntity());
				Log.i("fsd", output);

			} catch (IllegalStateException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}

			if (output == null) {

				try {
					responceJson.put("Success", false);
					responceJson.put("Description",
							"The server didn't respond anything");
				} catch (JSONException e) {
					e.printStackTrace();
					return null;
				}

			} else {
				JSONObject json = null;
				try {
					json = new JSONObject(output);
				} catch (JSONException e1) {
					e1.printStackTrace();
					return null;
				}
				if (json == null) {
					try {
						responceJson.put("Success", false);
						responceJson.put("Description",
								"Error! The server responded is not readable");
					} catch (JSONException e) {
						e.printStackTrace();
						return null;
					}

				} else {
				}
			}
		}else  if (instruction.equals("bindGateway")) {
			String serialNumber;
			String gatewayID;
			try {
				serialNumber = (String) json.get("serialNumber");
				
			} catch (JSONException e3) {
				e3.printStackTrace();
				return null;
			}

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			nameValuePairs.add(new BasicNameValuePair("serial_number",
					serialNumber));

			SharedPreferences sharedPrefs = PreferenceManager
					.getDefaultSharedPreferences(context
							.getApplicationContext());

			HttpPost request = new HttpPost(GlobalConstants.ADVANCHIP_SERVER);
			String access_token = sharedPrefs.getString("access_token",
					"default");

			request.setHeader("Authorization", "Bearer " + access_token);
			try {
				request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			} catch (UnsupportedEncodingException e1) {
				Log.e("http post error",
						"some error occured with the http post request");
				e1.printStackTrace();
				return null;
			}

			// receiving the response
			HttpResponse response = null;
			HttpParams httpParams = new BasicHttpParams();

			KeyStore localTrustStore = null;
			try {
				localTrustStore = KeyStore.getInstance("BKS");
			} catch (KeyStoreException e1) {
				e1.printStackTrace();
				return null;
			}
			InputStream in = context.getResources().openRawResource(
					R.raw.advanchip_trust_store);
			try {
				localTrustStore.load(in, "".toCharArray());
			} catch (NoSuchAlgorithmException e2) {
				e2.printStackTrace();
				return null;
			} catch (CertificateException e2) {
				e2.printStackTrace();
				return null;
			} catch (IOException e2) {
				e2.printStackTrace();
				return null;
			}
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			SSLSocketFactory sslSocketFactory = null;
			try {
				sslSocketFactory = new SSLSocketFactory(localTrustStore);
			} catch (KeyManagementException e1) {

				e1.printStackTrace();
				return null;
			} catch (UnrecoverableKeyException e1) {
				e1.printStackTrace();
				return null;
			} catch (NoSuchAlgorithmException e1) {
				e1.printStackTrace();
				return null;
			} catch (KeyStoreException e1) {
				e1.printStackTrace();
				return null;
			}
			schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));

			ClientConnectionManager cm = new ThreadSafeClientConnManager(
					httpParams, schemeRegistry);

			HttpClient client = new DefaultHttpClient(cm, httpParams);

			try {
				response = client.execute(request);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String output = null;
			try {
				output = EntityUtils.toString(response.getEntity());
				Log.i("fsd", output);

			} catch (IllegalStateException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}

			if (output == null) {

				try {
					responceJson.put("Success", false);
					responceJson.put("Description",
							"The server didn't respond anything");
				} catch (JSONException e) {
					e.printStackTrace();
					return null;
				}

			} else {
				JSONObject json = null;
				try {
					json = new JSONObject(output);
				} catch (JSONException e1) {
					e1.printStackTrace();
					return null;
				}
			
			}
		}
		return responceJson;

	}

}
