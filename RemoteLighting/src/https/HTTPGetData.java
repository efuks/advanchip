package https;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.advanchiplighting.remotelighting.GlobalConstants;
import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.account.AccountControl;

public class HTTPGetData {

	private static boolean requestIsComplete = true;
	private static String output = null;

	public HTTPGetData() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	static public String fetchGatewayData(JSONObject jsonInstruction,
			Context context) {

		// Add your data
		String request = null;
		String gatewayID = null;
		String floorID = null;
		String roomID = null;

		String instruction = null;
		try {
			instruction = (String) jsonInstruction.getString("instruction");
		} catch (JSONException e3) {

			e3.printStackTrace();
			return output;
		}

		if (instruction.equals("gateways")) {
			request = GlobalConstants.ADVANCHIP_SERVER;

		} else if (instruction.equals("gateway")) {

			try {
				gatewayID = (String) jsonInstruction.getString("gatewayID");
				request = GlobalConstants.ADVANCHIP_SERVER + "/" + gatewayID;
			} catch (JSONException e) {

				e.printStackTrace();
				return output;
			}

		} else if (instruction.equals("gates")) {

			try {
				gatewayID = (String) jsonInstruction.getString("gatewayID");
				request = GlobalConstants.ADVANCHIP_SERVER + "/" + gatewayID
						+ "/gates";
			} catch (JSONException e) {

				e.printStackTrace();
				return output;
			}

		} else if (instruction.equals("floors")) {

			try {
				gatewayID = (String) jsonInstruction.getString("gatewayID");
				request = GlobalConstants.ADVANCHIP_SERVER + "/" + gatewayID
						+ "/floors";
			} catch (JSONException e) {

				e.printStackTrace();
				return output;
			}

		} else if (instruction.equals("rooms")) {

			try {
				gatewayID = (String) jsonInstruction.getString("gatewayID");
				floorID = (String) jsonInstruction.getString("floorID");
				request = GlobalConstants.ADVANCHIP_SERVER + "/" + gatewayID
						+ "/floors/" + floorID + "/rooms";
			} catch (JSONException e) {

				e.printStackTrace();
				return output;
			}

		} else if (instruction.equals("switches")) {

			try {
				gatewayID = (String) jsonInstruction.getString("gatewayID");
				floorID = (String) jsonInstruction.getString("floorID");
				roomID = (String) jsonInstruction.getString("roomID");
				request = GlobalConstants.ADVANCHIP_SERVER + "/" + gatewayID
						+ "/floors/" + floorID + "/rooms/" + roomID + "/gates";
			
			} catch (JSONException e) {

				e.printStackTrace();
				return output;
			}

		}

		HttpParams httpParams = new BasicHttpParams();

		KeyStore localTrustStore = null;
		try {
			localTrustStore = KeyStore.getInstance("BKS");
		} catch (KeyStoreException e1) {

			e1.printStackTrace();
			return output;
		}

		InputStream in = context.getResources().openRawResource(
				R.raw.advanchip_trust_store);
		try {
			localTrustStore.load(in, "".toCharArray());
		} catch (NoSuchAlgorithmException e2) {

			e2.printStackTrace();
			return output;
		} catch (CertificateException e2) {

			e2.printStackTrace();
			return output;
		} catch (IOException e2) {

			e2.printStackTrace();
			return output;
		}
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		SSLSocketFactory sslSocketFactory = null;
		try {
			sslSocketFactory = new SSLSocketFactory(localTrustStore);
			sslSocketFactory
					.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		} catch (KeyManagementException e1) {

			e1.printStackTrace();
			return output;
		} catch (UnrecoverableKeyException e1) {

			e1.printStackTrace();
			return output;
		} catch (NoSuchAlgorithmException e1) {

			e1.printStackTrace();
			return output;
		} catch (KeyStoreException e1) {

			e1.printStackTrace();
			return output;
		}
		schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));

		ClientConnectionManager cm = new ThreadSafeClientConnManager(
				httpParams, schemeRegistry);

		Log.i("Server request: ", request);
		String requestAddress = request;

		do {
			requestIsComplete = true;
			StringBuilder url = new StringBuilder();
			url.append(requestAddress);
			HttpGet get = new HttpGet(url.toString());

			SharedPreferences sharedPrefs = PreferenceManager
					.getDefaultSharedPreferences(context
							.getApplicationContext());

			// int endOfSessionTime = sharedPrefs.getInt("expiresAt", 0);
			String access_token = sharedPrefs.getString("access_token",
					"default");

			get.setHeader("Authorization", "Bearer " + access_token);

			HttpClient client = new DefaultHttpClient(cm, httpParams);

			url.append(requestAddress);

			HttpUriRequest req = get;
			HttpResponse resp = null;
			try {
				resp = client.execute(req);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			HttpEntity httpEntity = resp.getEntity();

			try {
				output = EntityUtils.toString(resp.getEntity());
			} catch (ParseException e) {

				e.printStackTrace();
				return output;
			} catch (IOException e) {

				e.printStackTrace();
				return output;
			}
			Log.i("fsd", output);

			if (output.equals("Unauthorized")) {
				requestIsComplete = false;
				Log.i("refreshing the token",
						"we are refreshing the token for you");
				if (AccountControl.refresh_token(context)) {

					Log.i("token", "refreshed successfully");

				} else {
					// you are not authorised and need to login again
					return "relogin";
				}
			}
		} while (!requestIsComplete);

		return output;
	}
}
