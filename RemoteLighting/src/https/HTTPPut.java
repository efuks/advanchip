package https;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;

import com.advanchiplighting.remotelighting.GlobalConstants;
import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.account.AccountControl;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.util.Log;

public class HTTPPut {

	Context context;
	JSONObject json;

	public HTTPPut(Context context, JSONObject json) {
		this.context = context;
		this.json = json;

	}

	public JSONObject runHTTPPut() {
		JSONObject result = new JSONObject();
		if (json == null) {

			try {
				result.put("success", false);
				result.put("description", "null instruction");
			} catch (JSONException e) {

				e.printStackTrace();
				return result;
			}
			return result;
		} else {
			String instr = null;
			try {
				instr = json.getString("instruction");
			} catch (JSONException e4) {

				e4.printStackTrace();
				return result;
			}

			if (instr.equals("changePermissions")) {

				String permission = null;
				String gatewayID = null;
				String userID = null;
				try {
					permission = json.getString("permissions");
					gatewayID = json.getString("gatewayID");
					userID = json.getString("userID");
				} catch (JSONException e3) {

					e3.printStackTrace();
					return result;
				}

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				nameValuePairs.add(new BasicNameValuePair("level", permission));
				HttpPut request = new HttpPut(
						"https://www.advanchiplighting.com/api/v1/gateways/"
								+ gatewayID + "/permissions/" + userID);

				SharedPreferences sharedPrefs = PreferenceManager
						.getDefaultSharedPreferences(context
								.getApplicationContext());
				String access_token = sharedPrefs.getString("access_token",
						"default");
				request.setHeader("Authorization", "Bearer " + access_token);
				try {
					request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				} catch (UnsupportedEncodingException e1) {
					Log.e("http post error",
							"some error occured with the http post request");
					e1.printStackTrace();
				}

				// receiving the response
				HttpResponse response = null;
				HttpParams httpParams = new BasicHttpParams();

				KeyStore localTrustStore = null;
				try {
					localTrustStore = KeyStore.getInstance("BKS");
				} catch (KeyStoreException e1) {

					e1.printStackTrace();
					return result;
				}
				InputStream in = context.getResources().openRawResource(
						R.raw.advanchip_trust_store);
				try {
					localTrustStore.load(in, "".toCharArray());
				} catch (NoSuchAlgorithmException e2) {

					e2.printStackTrace();
					return result;
				} catch (CertificateException e2) {

					e2.printStackTrace();
					return result;
				} catch (IOException e2) {

					e2.printStackTrace();
					return result;
				}
				SchemeRegistry schemeRegistry = new SchemeRegistry();
				schemeRegistry.register(new Scheme("http", PlainSocketFactory
						.getSocketFactory(), 80));
				SSLSocketFactory sslSocketFactory = null;
				try {
					sslSocketFactory = new SSLSocketFactory(localTrustStore);
				} catch (KeyManagementException e1) {

					e1.printStackTrace();
					return result;
				} catch (UnrecoverableKeyException e1) {

					e1.printStackTrace();
					return result;
				} catch (NoSuchAlgorithmException e1) {

					e1.printStackTrace();

					return result;
				} catch (KeyStoreException e1) {

					e1.printStackTrace();
					return result;
				}
				schemeRegistry.register(new Scheme("https", sslSocketFactory,
						443));

				ClientConnectionManager cm = new ThreadSafeClientConnManager(
						httpParams, schemeRegistry);

				HttpClient client = new DefaultHttpClient(cm, httpParams);

				try {
					response = client.execute(request);
				} catch (Exception e) {
					e.printStackTrace();
					return result;
				}

				HttpEntity httpEntity = response.getEntity();

				String output = null;
				try {
					output = EntityUtils.toString(response.getEntity());
					Log.i("log", output);
				} catch (ParseException e) {

					e.printStackTrace();
					return result;
				} catch (IOException e) {

					e.printStackTrace();
					return result;
				}

				String messageDialog = null;
				if (output == null) {
					messageDialog = "empty response";
				} else if (output
						.equals("User is not an admin of this gateway")) {
					messageDialog = "User is not an admin of this gateway";
				} else {
					JSONObject json = null;
					try {
						json = new JSONObject(output);

						if (json.has("userDisplayName")) {

							permission = "unknown";
							int level = json.getInt("level");
							if (level == 1) {
								permission = "basic";
							} else if (level == 2) {
								permission = "administrator";
							}
							messageDialog = json.getString("userDisplayName")
									+ " has " + permission + " control";
						}

					} catch (JSONException e) {
						Log.i("permission change", "bad json " + output);
						e.printStackTrace();
						return result;
					}
				}

				try {

					result.put("description", "successfully changed permission");
					result.put("success", true);

				} catch (JSONException e) {

					e.printStackTrace();
					return result;
				}

				// AlertDialog dialog = new AlertDialog.Builder(context)
				// .setMessage(messageDialog).setTitle("Changing permissions")
				// .setPositiveButton("Ok", null).create();
				// dialog.show();

			} else if (instr.equals("toggleLight")) {

				String gateID = null;
				String gatewayID = null;
				String state = null;
				try {
					gateID = json.getString("gateID");
					gatewayID = json.getString("gatewayID");
					state = json.getString("state");
				} catch (JSONException e3) {

					e3.printStackTrace();
					return result;
				}

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				nameValuePairs.add(new BasicNameValuePair("state", state));
				HttpPut request = new HttpPut(
						"https://www.advanchiplighting.com/api/v1/gateways/"
								+ gatewayID + "/gates/" + gateID);

				SharedPreferences sharedPrefs = PreferenceManager
						.getDefaultSharedPreferences(context
								.getApplicationContext());
				String access_token = sharedPrefs.getString("access_token",
						"default");
				request.setHeader("Authorization", "Bearer " + access_token);
				try {
					request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				} catch (UnsupportedEncodingException e1) {
					Log.e("http post error",
							"some error occured with the http post request");
					e1.printStackTrace();
				}

				// receiving the response
				HttpResponse response = null;
				HttpParams httpParams = new BasicHttpParams();

				KeyStore localTrustStore = null;
				try {
					localTrustStore = KeyStore.getInstance("BKS");
				} catch (KeyStoreException e1) {

					e1.printStackTrace();
					return result;
				}
				InputStream in = context.getResources().openRawResource(
						R.raw.advanchip_trust_store);
				try {
					localTrustStore.load(in, "".toCharArray());
				} catch (NoSuchAlgorithmException e2) {

					e2.printStackTrace();
					return result;
				} catch (CertificateException e2) {

					e2.printStackTrace();
					return result;
				} catch (IOException e2) {

					e2.printStackTrace();
					return result;
				}
				SchemeRegistry schemeRegistry = new SchemeRegistry();
				schemeRegistry.register(new Scheme("http", PlainSocketFactory
						.getSocketFactory(), 80));
				SSLSocketFactory sslSocketFactory = null;
				try {
					sslSocketFactory = new SSLSocketFactory(localTrustStore);
				} catch (KeyManagementException e1) {

					e1.printStackTrace();
					return result;
				} catch (UnrecoverableKeyException e1) {

					e1.printStackTrace();
					return result;
				} catch (NoSuchAlgorithmException e1) {

					e1.printStackTrace();

					return result;
				} catch (KeyStoreException e1) {

					e1.printStackTrace();
					return result;
				}
				schemeRegistry.register(new Scheme("https", sslSocketFactory,
						443));

				ClientConnectionManager cm = new ThreadSafeClientConnManager(
						httpParams, schemeRegistry);

				HttpClient client = new DefaultHttpClient(cm, httpParams);

				try {
					response = client.execute(request);
				} catch (Exception e) {
					e.printStackTrace();
					return result;
				}

				HttpEntity httpEntity = response.getEntity();

				String output = null;
				try {
					output = EntityUtils.toString(response.getEntity());
					Log.i("log", output);
				} catch (ParseException e) {

					e.printStackTrace();
					return result;
				} catch (IOException e) {

					e.printStackTrace();
					return result;
				}

				String messageDialog = null;
				if (output == null) {
					messageDialog = "empty response";
				} else if (output
						.equals("User is not an admin of this gateway")) {
					messageDialog = "User is not an admin of this gateway";
				} else {   }

				try {

					result.put("description", "successfully changed permission");
					result.put("success", true);

				} catch (JSONException e) {

					e.printStackTrace();
					return result;
				}

				// AlertDialog dialog = new AlertDialog.Builder(context)
				// .setMessage(messageDialog).setTitle("Changing permissions")
				// .setPositiveButton("Ok", null).create();
				// dialog.show();

			}
			else if (instr.equals("changeLightName")) {

			}else if (instr.equals("changeName")){}

		}
		return result;

	}

}
