package com.advanchiplighting.remotelighting.management;

import https.HTTPPostRequest;
import https.HTTPPut;

import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

public class ChangeObjectName extends AsyncTask<Void, Void, JSONObject> {

	Context context;
	JSONObject json;

	public ChangeObjectName(Context context, JSONObject json) {
		this.context = context;
		this.json = json;
	}

	@Override
	protected JSONObject doInBackground(Void... params) {

		HTTPPut put =new HTTPPut(context, json);
		put.runHTTPPut();
		
		return null;
	}

}