package com.advanchiplighting.remotelighting.management;

import org.holoeverywhere.app.Fragment;

import com.advanchiplighting.remotelighting.BaseActivity;
import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.R.id;
import com.advanchiplighting.remotelighting.locationsfragment.UserListFragment;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

public class ManageUserActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle extra = getIntent().getExtras();
		String gatewayID = extra.getString("gatewayID");
		
		Bundle bundle = new Bundle();
		bundle.putString("gatewayID", gatewayID);
		Fragment fragment = new UserListFragment();
		fragment.setArguments(bundle);
		FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, fragment);
		transaction.commit();
	}

}
