package com.advanchiplighting.remotelighting.management;

import java.util.ArrayList;

import https.HTTPGetData;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.account.AccountControl;
import com.advanchiplighting.remotelighting.account.LoginActivity;
import com.advanchiplighting.remotelighting.customlist.LocationAdapter2;
import com.advanchiplighting.remotelighting.customlist.LocationItem2;
import com.advanchiplighting.remotelighting.locationsfragment.Binding;
import com.ti.cc3x.android.CC3XConfigActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.AdapterView.OnItemClickListener;

/**
 * This activity displays setting options such as add the gateway, configure the
 * wi-fi for the gateway, add the floors and add the rooms
 * 
 * @author Edward
 * 
 */
public class ManageGatewayFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.list_two_button);
		new FetchGatewaysTask().execute();

		Button button1 = (Button) view.findViewById(R.id.two_button_1);
		button1.setText("Configure Gateway");
		button1.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getActivity(),
						CC3XConfigActivity.class);
				startActivity(intent);
			}
		});

		Button button2 = (Button) view.findViewById(R.id.two_button_2);
		button2.setText("Add Gateway");
		button2.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				final Dialog addGateway = new Dialog(getActivity());
				addGateway.setContentView(R.layout.dialog_add_gateway);
				addGateway.setTitle("Enter New Gateway ID");

				final EditText gatewayId = (EditText) addGateway
						.findViewById(R.id.dialog_gateway_id);

				Button ok = (Button) addGateway
						.findViewById(R.id.dialog_gateway_ok);
				Button cancel = (Button) addGateway
						.findViewById(R.id.dialog_gateway_cancel);

				ok.setOnClickListener(new Button.OnClickListener() {

					@Override
					public void onClick(View v) {
						String inputId = gatewayId.getText().toString();
						JSONObject data = new JSONObject();

						try {

							data.put("instruction", "bindGateway");
							data.put("serialNumber", inputId);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Binding bind = new Binding(getActivity(), data);
						bind.execute();

						addGateway.dismiss();

					}

				});

				cancel.setOnClickListener(new Button.OnClickListener() {

					@Override
					public void onClick(View v) {
						addGateway.dismiss();
					}
				});

				addGateway.show();
			}
		});

		return view;
	}

	private class FetchGatewaysTask extends
			AsyncTask<Void, Void, ArrayList<LocationItem2>> {
		private ManageGatewayFragment fragment = ManageGatewayFragment.this;

		@Override
		protected void onPreExecute() {
			// showProgressBar();
		}

		@Override
		protected ArrayList<LocationItem2> doInBackground(Void... params) {
			String output;
			JSONObject requestJson = new JSONObject();

			try {
				requestJson.put("instruction", "gateways");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			output = HTTPGetData.fetchGatewayData(requestJson, getActivity());
			if (output.equals("relogin")) {
				// you need to relogin;

				return null;
			}
			JSONArray jsonArray = null;
			try {
				jsonArray = new JSONArray(output);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Log.i("json", jsonArray.toString());
			ArrayList<LocationItem2> gateways = new ArrayList<LocationItem2>();
			for (int i = 0; i < jsonArray.length(); i++) {
				try {

					String name = (String) jsonArray.getJSONObject(i).get(
							"name");

					String id = (String) jsonArray.getJSONObject(i).get("_id");
					LocationItem2 item = new LocationItem2();
					item.setId(id);
					item.setName(name);
					gateways.add(item);

				} catch (JSONException e) {
					Log.e("json", "error reading json");
					e.printStackTrace();
				}
			}
			Log.i("fetching", "fetching is complete");
			return gateways;
		}

		@Override
		protected void onPostExecute(ArrayList<LocationItem2> gateways) {
			if (gateways == null) {
				AccountControl.relogin(getActivity());
			} else
				// hideProgressBar();
				setGatewayListView(gateways);
		}

		private void setGatewayListView(ArrayList<LocationItem2> gateways) {
			ListView listview = (ListView) fragment.getView().findViewById(
					R.id.two_button_listview);
			LocationAdapter2 adapter = new LocationAdapter2(
					fragment.getActivity(), R.layout.item_one_string, gateways);

			listview.setAdapter(adapter);
			listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					LocationItem2 item = (LocationItem2) arg0.getAdapter()
							.getItem(arg2);
					Fragment fragment = new ManageFloorFragment();
					Bundle bundle = new Bundle();
					bundle.putString("gatewayID", item.getId());
					fragment.setArguments(bundle);
					FragmentTransaction transaction = getActivity()
							.getSupportFragmentManager().beginTransaction();
					transaction.replace(R.id.content_frame, fragment);
					transaction.addToBackStack(null);
					transaction.commit();
				}
			});
			listview.setOnItemLongClickListener(new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					LocationItem2 item = (LocationItem2) arg0.getAdapter()
							.getItem(arg2);
					Log.i("longclick", item.getName());

					final Dialog editGateway = new Dialog(getActivity());
					editGateway.setContentView(R.layout.dialog_edit_location);
					editGateway.setTitle("Edit Gateway");
					EditText name = (EditText) editGateway
							.findViewById(R.id.dialog_edit_location_name);
					name.setText(item.getName());

					String newName = name.getText().toString();

					Button delete = (Button) editGateway
							.findViewById(R.id.dialog_edit_location_delete);
					Button ok = (Button) editGateway
							.findViewById(R.id.dialog_edit_location_ok);
					Button cancel = (Button) editGateway
							.findViewById(R.id.dialog_edit_location_cancel);

					delete.setOnClickListener(new Button.OnClickListener() {

						@Override
						public void onClick(View arg0) {
							// TODO delete the location

						}
					});

					ok.setOnClickListener(new Button.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO save updated name
							editGateway.dismiss();
						}
					});

					cancel.setOnClickListener(new Button.OnClickListener() {

						@Override
						public void onClick(View v) {
							editGateway.dismiss();
						}
					});

					editGateway.show();

					return true;
				}
			});
		}

		private void showProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (!progressBar.isShown()) {
				progressBar.setVisibility(View.VISIBLE);
			}
		}

		private void hideProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (progressBar.isShown()) {
				progressBar.setVisibility(View.GONE);
			}
		}
	}

}
