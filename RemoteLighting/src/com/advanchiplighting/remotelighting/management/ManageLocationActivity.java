package com.advanchiplighting.remotelighting.management;

import org.holoeverywhere.app.Fragment;

import com.advanchiplighting.remotelighting.BaseActivity;
import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.R.id;

import android.os.Bundle;

/**
 * This activity contains the fragment for the settings of the gateway 9to add
 * the gateway or configure the gateway's wi-fi)
 * 
 * @author John Lee
 * 
 */
public class ManageLocationActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fragment fragment = new ManageGatewayFragment();
		this.getSupportFragmentManager().beginTransaction()
				.replace(R.id.content_frame, fragment).commit();
	}

}
