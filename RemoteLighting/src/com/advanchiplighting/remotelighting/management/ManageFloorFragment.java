package com.advanchiplighting.remotelighting.management;

import java.util.ArrayList;

import https.HTTPGetData;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.ProgressBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.R.id;
import com.advanchiplighting.remotelighting.R.layout;
import com.advanchiplighting.remotelighting.account.AccountControl;
import com.advanchiplighting.remotelighting.customlist.LocationAdapter2;
import com.advanchiplighting.remotelighting.customlist.LocationItem2;
import com.advanchiplighting.remotelighting.newrequests.NewFloorRequest;

import com.ti.cc3x.android.CC3XConfigActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

/**
 * This activity displays setting options such as add the gateway, configure the
 * wi-fi for the gateway, add the floors and add the rooms
 * 
 * @author Edward
 * 
 */
public class ManageFloorFragment extends Fragment {

	String requestedGateway;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.list_one_button);
		FetchGatewaysTask task = new FetchGatewaysTask("floors", null);
		task.execute();
		Bundle bundle = this.getArguments();
		Log.i("info", bundle.getString("gatewayID"));
		requestedGateway = bundle.getString("gatewayID");

		Button button = (Button) view.findViewById(R.id.one_button);
		button.setText("Add Floor");
		button.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {

				final Dialog addGateway = new Dialog(getActivity());
				addGateway.setContentView(R.layout.dialog_add_gateway);
				addGateway.setTitle("Enter New Floor Name");

				final EditText floorName = (EditText) addGateway
						.findViewById(R.id.dialog_gateway_id);

				Button ok = (Button) addGateway
						.findViewById(R.id.dialog_gateway_ok);
				Button cancel = (Button) addGateway
						.findViewById(R.id.dialog_gateway_cancel);

				ok.setOnClickListener(new Button.OnClickListener() {

					@Override
					public void onClick(View v) {
						String name = floorName.getText().toString();

						addGateway.dismiss();
						JSONObject request = new JSONObject();
						try {
							request.put("name", name);
							request.put("gatewayID", requestedGateway);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						FetchGatewaysTask task = new FetchGatewaysTask(
								"newFloor", request);
						task.execute();
						addGateway.dismiss();

					}

				});

				cancel.setOnClickListener(new Button.OnClickListener() {

					@Override
					public void onClick(View v) {
						addGateway.dismiss();
					}
				});

				addGateway.show();
			}
		});

		return view;
	}

	private class FetchGatewaysTask extends AsyncTask<Void, Void, Object> {

		String instruction;
		JSONObject request;

		public FetchGatewaysTask(String instruction, JSONObject details) {
			this.instruction = instruction;
			this.request = details;

		}

		private ManageFloorFragment fragment = ManageFloorFragment.this;

		@Override
		protected void onPreExecute() {
			// showProgressBar();
		}

		@Override
		protected Object doInBackground(Void... params) {

			String output;
			JSONArray json = null;

			JSONObject requestJson = new JSONObject();

			if (instruction.equals("floors")) {
				try {
					requestJson.put("instruction", "floors");
					requestJson.put("gatewayID", requestedGateway);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				output = HTTPGetData.fetchGatewayData(requestJson,
						getActivity());
				try {
					json = new JSONArray(output);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				JSONObject tree = null;

				ArrayList<LocationItem2> floors = new ArrayList<LocationItem2>();
				for (int i = 0; i < json.length(); i++) {
					try {

						String name = (String) json.getJSONObject(i)
								.get("name");

						String id = (String) json.getJSONObject(i).get("_id");
						LocationItem2 item = new LocationItem2();
						item.setId(id);
						item.setName(name);
						floors.add(item);

					} catch (JSONException e) {
						Log.e("json", "error reading json");
						e.printStackTrace();
					}
				}
				Log.i("fetching", "fetching is complete");
				return floors;
			} else if (instruction.equals("newFloor")) {

				String floorname = null;
				try {
					floorname = (String) (request.get("name"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				NewFloorRequest task = new NewFloorRequest(requestedGateway,
						getActivity(), floorname);
				JSONObject result = task.addTheNewFloor();

				return result;

			}
			return null;
		}

		@Override
		protected void onPostExecute(Object object) {

			if (instruction.equals("floors")) {
				ArrayList<LocationItem2> floors = (ArrayList<LocationItem2>) object;
				if (floors == null) {
					AccountControl.relogin(getActivity());
				} else {
					setGatewayListView(floors);
				}
			}else if (instruction.equals("newFloor")) {

				JSONObject result = (JSONObject) object;
				Boolean success = false;
				try {
					success = (Boolean) result.get("Success");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (success){
					String name = null;
					try {
						name = result.getString("name");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String dialogMesage=name+" was successfully added";
					AlertDialog dialog = new AlertDialog.Builder(getActivity())
					.setMessage(dialogMesage).setTitle("Success!")
					.setPositiveButton("Ok", null).create();
			dialog.show();
			FetchGatewaysTask task = new FetchGatewaysTask("floors", null);
			task.execute();
				}else{
					String dialogMesage = null;
					try {
						
						dialogMesage = result.getString("Description");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					AlertDialog dialog = new AlertDialog.Builder(getActivity())
					.setMessage(dialogMesage).setTitle("Failed!")
					.setPositiveButton("Ok", null).create();
			dialog.show();
					
				}

			}
		}

		private void setGatewayListView(ArrayList<LocationItem2> floors) {
			ListView listview = (ListView) fragment.getView().findViewById(
					R.id.one_button_listview);
			LocationAdapter2 adapter = new LocationAdapter2(
					fragment.getActivity(), R.layout.item_one_string, floors);

			listview.setAdapter(adapter);
			listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					LocationItem2 item = (LocationItem2) arg0.getAdapter()
							.getItem(arg2);

					Fragment fragment = new ManageRoomFragment();
					Bundle bundle = new Bundle();
					bundle.putString("floorID", item.getId());
					bundle.putString("gatewayID", requestedGateway);
					fragment.setArguments(bundle);
					FragmentTransaction transaction = getActivity()
							.getSupportFragmentManager().beginTransaction();
					transaction.replace(R.id.content_frame, fragment);
					transaction.addToBackStack(null);
					transaction.commit();

				}
			});
			listview.setOnItemLongClickListener(new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					LocationItem2 item = (LocationItem2) arg0.getAdapter().getItem(arg2);
					Log.i("longclick",item.getName());
					
					final Dialog editFloor = new Dialog(getActivity());
					editFloor.setContentView(R.layout.dialog_edit_location);
					editFloor.setTitle("Edit Floor");
					EditText name = (EditText) editFloor.findViewById(R.id.dialog_edit_location_name);
					name.setText(item.getName());
					
					String newName = name.getText().toString();
					
					Button delete = (Button) editFloor.findViewById(R.id.dialog_edit_location_delete);
					Button ok = (Button) editFloor.findViewById(R.id.dialog_edit_location_ok);
					Button cancel = (Button) editFloor.findViewById(R.id.dialog_edit_location_cancel);
					
					delete.setOnClickListener(new Button.OnClickListener() {
						
						@Override
						public void onClick(View arg0) {
							// TODO delete the location
							
						}
					});
					
					ok.setOnClickListener(new Button.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO save updated name
							editFloor.dismiss();
						}
					});
					
					cancel.setOnClickListener(new Button.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							editFloor.dismiss();
						}
					});
					
 					
					editFloor.show();
					
					return true;
				}
			});
		}

		private void showProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (!progressBar.isShown()) {
				progressBar.setVisibility(View.VISIBLE);
			}
		}

		private void hideProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (progressBar.isShown()) {
				progressBar.setVisibility(View.GONE);
			}
		}
	}

}
