package com.advanchiplighting.remotelighting;

import org.holoeverywhere.app.Fragment;

import com.advanchiplighting.remotelighting.locationsfragment.ConsumerLightsFragment;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

/**
 * This class displays the list of the gateways in the light controll menu
 * 
 * @author Edward Fuks
 * 
 */
public class LocationsActivity extends BaseActivity {

	protected String access_token;
	protected String refresh_token;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.access_token = super.shared_preferences.getString("access_token",
				"Default");
		this.refresh_token = super.shared_preferences.getString(
				"refresh_token", "Default");
		
		
		
		
		
		
		
		Fragment fragment = new ConsumerLightsFragment();
		Bundle bundle = new Bundle();
		bundle.putString("instruction", "gateways");
		fragment.setArguments(bundle);
		FragmentTransaction transaction = this
				.getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, fragment);
		transaction.addToBackStack(null);
		transaction.commit();

	}

	

	@Override
	public void onBackPressed() {

		if (backBtnEnabled) {
			Log.i("back button", " has been pressed");

			super.onBackPressed();

		}
	}

}
