package com.advanchiplighting.remotelighting.switchlisttab;

import https.HTTPGetData;

import java.util.ArrayList;
import java.util.HashMap;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.ProgressBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.account.AccountControl;
import com.advanchiplighting.remotelighting.customlist.SwitchAdapter;
import com.advanchiplighting.remotelighting.customlist.SwitchItem;
import com.advanchiplighting.remotelighting.locationsfragment.SwitchesActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class AllSwitchFragment extends Fragment {

	private static final Integer USER_ITEM = 0;
	private static final Integer ADD_USER_ITEM = 1;
	private String requestedGateway;
	private String requestedRoom;
	private String requestedFloor;
	private String instruction;

	public AllSwitchFragment(String instr) {
		this.instruction=instr;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.tab_switch_list, container, false);

		SwitchesActivity a = (SwitchesActivity) this.getActivity();
		requestedGateway = a.getGateway();
		requestedRoom = a.getRequestedRoom();
		requestedFloor = a.getRequestedFloor();
		getActivity().setTitle("switches");

		Button allOn = (Button) view.findViewById(R.id.all_on);
		allOn.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Log.i("allOn", "implement the method in allswitch fragment");

			}
		});

		Button allOff = (Button) view.findViewById(R.id.all_off);
		allOff.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				Log.i("allFF", "implement the method in allswitch fragment");
			}
		});

		new FetchSwitchTaask().execute();
		

		return view;

	}

	private class FetchSwitchTaask extends
			AsyncTask<Void, Void, ArrayList<SwitchItem>> {
		private AllSwitchFragment fragment = AllSwitchFragment.this;

		@Override
		protected void onPreExecute() {
			// showProgressBar();
		}

		@Override
		protected ArrayList<SwitchItem> doInBackground(Void... params) {

			String output;
			JSONArray json = null;
			JSONObject requestJson = new JSONObject();

/*		try {
				requestJson.put("instruction", "switches");
				requestJson.put("gatewayID", requestedGateway);
				requestJson.put("roomID", requestedRoom);
				requestJson.put("floorID", requestedFloor);
			} catch (JSONException e) {
				
				e.printStackTrace();
				return new ArrayList<SwitchItem>();
			}
			output = HTTPGetData.fetchGatewayData(requestJson, getActivity());
			try {
				json = new JSONArray(output);
				
			} catch (JSONException e1) {

				e1.printStackTrace();
				return new ArrayList<SwitchItem>();

			}
			HashMap hm = null;
			if (json != null) {
				hm = new HashMap();
			}
			if (json.length() == 0) {
				return new ArrayList<SwitchItem>();
			}
			String switchID = null;
			for (int i = 0; i < json.length(); i++) {
				try {
					switchID = json.getString(i);
				} catch (JSONException e) {
					
					e.printStackTrace();
					return new ArrayList<SwitchItem>();
				}
				hm.put(switchID, switchID);
			}
*/
			requestJson = new JSONObject();

			try {
				requestJson.put("instruction", "gateway");
				requestJson.put("gatewayID", requestedGateway);

			} catch (JSONException e) {

				e.printStackTrace();
				return new ArrayList<SwitchItem>();
			}

			output = HTTPGetData.fetchGatewayData(requestJson, getActivity());
			JSONArray jsonArray = null;

			JSONObject totalGateway;
			try {
				totalGateway = new JSONObject(output);
			} catch (JSONException e1) {

				e1.printStackTrace();
				return new ArrayList<SwitchItem>();
			}

			try {
				jsonArray = totalGateway.getJSONArray("gates");
			} catch (JSONException e) {

				e.printStackTrace();
				return new ArrayList<SwitchItem>();
			}

			String name = null;
			String id = null;
			

			ArrayList<SwitchItem> switches = new ArrayList<SwitchItem>();
			for (int i = 0; i < jsonArray.length(); i++) {

				
				boolean stateBool = false;
				int stateInt = 0;
				try {
					
					id = (String) jsonArray.getJSONObject(
							i).get("_id");
				} catch (JSONException e) {

					e.printStackTrace();
					
				}
				
				
				//if (hm.get(id)!=null){
					
					try {
						name=(String)jsonArray.getJSONObject(
								i).get("name");
						stateInt=(Integer)jsonArray.getJSONObject(
								i).get("state");
					} catch (JSONException e) {
						
						e.printStackTrace();
						return new ArrayList<SwitchItem>();
					}
					if (stateInt==0){
						stateBool=false;
					}else if (stateInt==1){
						stateBool=true;
					}
					
					if (instruction.endsWith("OFF")&& (!stateBool)){
						switches.add(new SwitchItem(id, requestedGateway, name, stateBool));
					}else if (instruction.endsWith("ON")&& (stateBool)){
						switches.add(new SwitchItem(id, requestedGateway, name, stateBool));
					}else if (instruction.endsWith("ALL")) {
						switches.add(new SwitchItem(id, requestedGateway, name, stateBool));
					}
					
			//	}
				
				
			}
			Log.i("fetching", "fetching is complete");
			return switches;
		}

		@Override
		protected void onPostExecute(ArrayList<SwitchItem> switches) {
			// hideProgressBar();
			if (switches == null) {
				AccountControl.relogin(getActivity());
			} else {
				setSwitchesList(switches);
			}
		}

		private void showProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (!progressBar.isShown()) {
				progressBar.setVisibility(View.VISIBLE);
			}
		}

		private void hideProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (progressBar.isShown()) {
				progressBar.setVisibility(View.GONE);
			}
		}

		private void setSwitchesList(ArrayList<SwitchItem> switches) {
			ListView listview = (ListView) fragment.getView().findViewById(
					R.id.tab_list);
			SwitchAdapter adapter = new SwitchAdapter(getActivity(),
					R.layout.item_switch, switches);

			listview.setAdapter(adapter);

		}
	}
}
