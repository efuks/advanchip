package com.advanchiplighting.remotelighting.account;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.advanchiplighting.remotelighting.GlobalConstants;
import com.advanchiplighting.remotelighting.MainActivity;
import com.advanchiplighting.remotelighting.R;

public class LoginActivity extends Activity {

	String dialogMesage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		hideProgressBar();
		bindLoginButton();
		
		this.bindLoginButton();
		this.bindSignupButton();
		this.bindForgotPwButton();
		getSupportActionBar().hide();
	}

	private void bindForgotPwButton() {
		TextView btn_forgot_pw = (TextView) findViewById(R.id.logIn_forgotPwClick);

		OnClickListener l = new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.d("Login Activity", "Go to Forgot Password");
				Intent intent = new Intent(LoginActivity.this,
						ForgotPasswordActivity.class);
				finish();
				startActivity(intent);

			}
		};
		btn_forgot_pw.setOnClickListener(l);
	}

	private void bindSignupButton() {
		TextView btn_sign_up = (TextView) findViewById(R.id.logIn_signUpClick);

		OnClickListener l = new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.d("Login Activity", "Go to Registration");
				Intent intent = new Intent(LoginActivity.this,
						RegistrationActivity.class);
				finish();
				startActivity(intent);

			}
		};
		btn_sign_up.setOnClickListener(l);
	}
	private void showProgressBar() {
		ProgressBar progressBar = (ProgressBar) 
				findViewById(R.id.fetch_gateways_progress);
		if (!progressBar.isShown()) {
			progressBar.setVisibility(View.VISIBLE);
		}
	}

	private void hideProgressBar() {
		ProgressBar progressBar = (ProgressBar) findViewById(R.id.fetch_gateways_progress);
		if (progressBar.isShown()) {
			progressBar.setVisibility(View.GONE);
		}
	}

	private void bindLoginButton() {
		final LoginActivity me = this;
		final Button login_btn = (Button) findViewById(R.id.logIn_button);

		// login
		login_btn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				login_btn.setEnabled(false);

				final String email = ((EditText) findViewById(R.id.logIn_email))
						.getText().toString();

				final String pwd = ((EditText) findViewById(R.id.logIn_password))
						.getText().toString();

				new AsyncTask<Void, Void, JSONObject>() {
					
					
					

					@Override
					protected void onPreExecute() {
						// TODO Auto-generated method stub
						showProgressBar();
						super.onPreExecute();
					}

					@Override
					protected JSONObject doInBackground(Void... params) {

						// Add your data

						JSONObject output = AccountControl.login(email, pwd,
								getBaseContext());
						return output;
					}

					@Override
					protected void onPostExecute(JSONObject output) {
						
						hideProgressBar();

						if (output == null) {
							dialogMesage = "Error! The server didn't respond anything";
						} else {
							JSONObject json = null;
							json = output;
							if (json == null) {
								dialogMesage = "Error! The server responded is not readable";
							} else {
								try {
									boolean jsonResult = json
											.getBoolean("Success");
									if (!jsonResult) {
										dialogMesage = "Error! "
												+ json.getString("Description");

									} else if (jsonResult) {

										Intent intent = new Intent(
												getBaseContext(),
												MainActivity.class);
										intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
										startActivity(intent);
										return;
									}
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
						}

						AlertDialog dialog = new AlertDialog.Builder(me)
								.setMessage(dialogMesage).setTitle("Failure!")
								.setPositiveButton("Ok", null).create();
						dialog.show();
						login_btn.setEnabled(true);

					}

				}.execute();

			}
		});

	}
}