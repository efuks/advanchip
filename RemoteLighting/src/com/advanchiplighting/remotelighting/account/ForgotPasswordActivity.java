package com.advanchiplighting.remotelighting.account;

import android.support.v7.app.ActionBar;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.TextView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.advanchiplighting.remotelighting.GlobalConstants;
import com.advanchiplighting.remotelighting.R;

public class ForgotPasswordActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgotpassword);

		this.bindLoginButton();
		this.bindRetrievePwordButton();
		getSupportActionBar().hide();
	}
	
	private void bindLoginButton(){
		TextView btn_login = (TextView) findViewById(R.id.forgotPword_login_Click);
		
		OnClickListener l = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d("forgotPasswordActivity", "Go to Login");
				Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
				finish();
				startActivity(intent);				
			}
		};
		btn_login.setOnClickListener(l);
	}
    
	
	private void bindRetrievePwordButton(){
		TextView btn_getPword = (TextView) findViewById(R.id.forgotPword_button);
		
		OnClickListener l = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String dialogMesage;
				String dialogTitle;
				boolean success = true; //temp variable just for the purpose of the if statement
				Log.d("forgotPasswordActivity", "retrieve password");
				
				
				//add code about retrieving password
				
				//if successful execute dialog
				if(success){
					dialogMesage = "Check your email to reset your password";//can change the words later
					dialogTitle = "Email Sent!";
				}else{
					dialogMesage = "Could not find your email. Please try again.";//can change the words later
					dialogTitle = "Failure";
				}
				
				AlertDialog dialog = new AlertDialog.Builder(ForgotPasswordActivity.this)
				.setMessage(dialogMesage).setTitle(dialogTitle)
				.setPositiveButton("Okay", null).create();
				dialog.show();
				
				
//				finish();
//				startActivity(intent);				
			}
		};
		btn_getPword.setOnClickListener(l);
	}
}
