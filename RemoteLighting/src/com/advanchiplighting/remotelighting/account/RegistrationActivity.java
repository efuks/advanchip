package com.advanchiplighting.remotelighting.account;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;

import org.apache.http.client.HttpClient;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.app.Activity;

import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.advanchiplighting.remotelighting.GlobalConstants;
import com.advanchiplighting.remotelighting.MainActivity;
import com.advanchiplighting.remotelighting.R;

public class RegistrationActivity extends Activity {

	private enum submitStatus {UNSUBMITTED, SUCCESS, PASSWORD_MISMATCH, EMPTY_NAME, EMPTY_EMAIL};
	private submitStatus regStatus = submitStatus.UNSUBMITTED;

	private String  dialogMesage;
	private JSONObject output;
	String firstName;

	 String lastName;

	 String password;

	 String displayNameText;

	 String emailAddress;

	 String passwordConfirm;
	 Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);

		bindNextButton();
		bindLoginButton();
		getSupportActionBar().hide();
		context=this;
	}

	private void bindLoginButton() {
		TextView btn_login = (TextView) findViewById(R.id.signUp_return_login);

		OnClickListener l = new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.d("RegistrationActivity", "Go to Login");
				Intent intent = new Intent(RegistrationActivity.this,
						LoginActivity.class);
				finish();
				startActivity(intent);
			}
		};
		btn_login.setOnClickListener(l);
	}

	
	private void showProgressBar() {
		ProgressBar progressBar = (ProgressBar) 
				findViewById(R.id.fetch_gateways_progress);
		if (!progressBar.isShown()) {
			progressBar.setVisibility(View.VISIBLE);
		}
	}

	private void hideProgressBar() {
		ProgressBar progressBar = (ProgressBar) findViewById(R.id.fetch_gateways_progress);
		if (progressBar.isShown()) {
			progressBar.setVisibility(View.GONE);
		}
	}
	
	private void bindNextButton() {
		final RegistrationActivity me = this;
		final Button button = (Button) findViewById(R.id.signUp_button);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				button.setEnabled(false);
				/*
				 * Ed or Jack: need to add first name, last name, and password
				 * confirmation to the database
				 */
				
				EditText fname =  (EditText) findViewById(R.id.signUp_fname);
				  firstName = fname.getText().toString();
				if (fname.length() == 0) {
					fname.setError("Required Field");
		        }
				
				EditText lname = (EditText) findViewById(R.id.signUp_lname);
				  lastName = lname.getText().toString();
				if (lname.length() == 0) {
					lname.setError("Required Field");
		        }
				
				 EditText email = (EditText) findViewById(R.id.signUp_email);
				  emailAddress = email.getText().toString();
				if (email.length() == 0) {
					email.setError("Required Field");
		        }
				
				EditText pword = (EditText) findViewById(R.id.signUp_password);
				   password = pword.getText().toString();
				if (pword.length() == 0) {
					pword.setError("Required Field");
		        }
				
				EditText pwordConf = (EditText) findViewById(R.id.signUp_confirmPassword);
				  passwordConfirm = pwordConf.getText().toString();
				if (pwordConf.length() == 0) {
					pwordConf.setError("Required Field");
		        }
				EditText displayeName = (EditText) findViewById(R.id.display_name);
				  displayNameText = pwordConf.getText().toString();
				if (displayeName.length() == 0) {
					displayeName.setError("Required Field");
		        }



				if (!password.equals(passwordConfirm)){
					Log.e("error","the passwords do not match!");
					//output = "failure";
					regStatus = submitStatus.PASSWORD_MISMATCH;

					
				}
				

				new AsyncTask<Void, Void, JSONObject>() {

					@Override
					protected JSONObject doInBackground(Void... params) {

						// Add your data

						JSONObject output = AccountControl.register(emailAddress, password, context, firstName, lastName, displayNameText);
						return output;
					
					
					}

					@Override
					protected void onPreExecute() {
						// TODO Auto-generated method stub
						//showProgressBar();
						super.onPreExecute();
					}
					
					@Override
					protected void onPostExecute(JSONObject output) {
						
						//hideProgressBar();

						if (output == null) {
							dialogMesage = "Error! The server didn't respond anything";
						} else {
							JSONObject json = null;
							json = output;
							if (json == null) {
								dialogMesage = "Error! The server responded is not readable";
							} else {
								try {
									boolean jsonResult = json
											.getBoolean("Success");
									if (!jsonResult) {
										dialogMesage = "Error! "
												+ json.getString("Description");

									} else if (jsonResult) {

										Intent intent = new Intent(
												getBaseContext(),
												MainActivity.class);
										intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
										startActivity(intent);
										return;
									}
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
						}

						AlertDialog dialog = new AlertDialog.Builder(me)
								.setMessage(dialogMesage).setTitle("Failure!")
								.setPositiveButton("Ok", null).create();
						dialog.show();
						//re.setEnabled(true);

					}

				}.execute();
			}
		});
	}

}
