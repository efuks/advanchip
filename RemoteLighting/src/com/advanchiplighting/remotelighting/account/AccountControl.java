package com.advanchiplighting.remotelighting.account;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.advanchiplighting.remotelighting.*;

public class AccountControl {

	public static JSONObject login(final String email, final String pwd,
			final Context context) {

		// Add your data

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("grant_type", "password"));
		nameValuePairs.add(new BasicNameValuePair("client_id", "android_v1"));
		nameValuePairs.add(new BasicNameValuePair("client_secret",
				GlobalConstants.CLIENT_SECRET));
		nameValuePairs.add(new BasicNameValuePair("username", email));
		nameValuePairs.add(new BasicNameValuePair("password", pwd));

		HttpPost request = new HttpPost(
				"https://www.advanchiplighting.com/oauth2/token");
		try {
			request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e1) {
			Log.e("http post error",
					"some error occured with the http post request");
			e1.printStackTrace();
		}

		// receiving the response
		HttpResponse response = null;
		HttpParams httpParams = new BasicHttpParams();

		KeyStore localTrustStore = null;
		try {
			localTrustStore = KeyStore.getInstance("BKS");
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InputStream in = context.getResources().openRawResource(
				R.raw.advanchip_trust_store);
		try {
			localTrustStore.load(in, "".toCharArray());
		} catch (NoSuchAlgorithmException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (CertificateException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		SSLSocketFactory sslSocketFactory = null;
		try {
			sslSocketFactory = new SSLSocketFactory(localTrustStore);
		} catch (KeyManagementException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnrecoverableKeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));

		ClientConnectionManager cm = new ThreadSafeClientConnManager(
				httpParams, schemeRegistry);

		HttpClient client = new DefaultHttpClient(cm, httpParams);

		try {
			response = client.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String output = null;
		try {
			output = EntityUtils.toString(response.getEntity());
			Log.i("fsd", output);

		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject responceJson = new JSONObject();
		if (output == null) {

			try {
				responceJson.put("Success", false);
				responceJson.put("Description",
						"The server didn't respond anything");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			JSONObject json = null;
			try {
				json = new JSONObject(output);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (json == null) {
				try {
					responceJson.put("Success", false);
					responceJson.put("Description",
							"Error! The server responded is not readable");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				try {
					if (json.has("error")) {

						responceJson.put("Success", false);
						responceJson.put("Description",
								json.getString("error_description"));

					} else if (json.has("access_token")) {
						responceJson.put("Success", true);
						responceJson.put("Description", "good");
						String access_token = json.getString("access_token");
						String refresh_token = json.getString("refresh_token");
						String expires_in = json.getString("expires_in");
						long timeOfExpiration = json.getLong("expires_in")
								+ System.currentTimeMillis() / 1000;
						String token_type = json.getString("token_type");

						SharedPreferences sharedPrefs = PreferenceManager
								.getDefaultSharedPreferences(context
										.getApplicationContext());
						SharedPreferences.Editor editor = sharedPrefs.edit();
						// editor.putString("username", email);
						editor.putString("username", email);
						editor.putString("password", pwd);
						editor.putString("access_token", access_token);
						editor.putString("refresh_token", refresh_token);
						editor.putString("expires_in", expires_in);
						editor.putString("token_type", token_type);
						editor.putLong("expiresAt", timeOfExpiration);

						editor.putBoolean("loggedIn", true).commit();

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		return responceJson;

	}

	public static JSONObject register(final String email, final String pwd,
			final Context context,String firstName, String lastName, String displayName) {

		// Add your data

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("grant_type", "password"));
		nameValuePairs.add(new BasicNameValuePair("client_id", "android_v1"));
		nameValuePairs.add(new BasicNameValuePair("client_secret",
				GlobalConstants.CLIENT_SECRET));
		nameValuePairs.add(new BasicNameValuePair("username", email));
		nameValuePairs.add(new BasicNameValuePair("password", pwd));
		nameValuePairs.add(new BasicNameValuePair("first_name", firstName));
		nameValuePairs.add(new BasicNameValuePair("last_name", lastName));
		nameValuePairs.add(new BasicNameValuePair("display_name", displayName));

		HttpPost request = new HttpPost(
				"https://www.advanchiplighting.com/api/v1/users");
		try {
			request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e1) {
			Log.e("http post error",
					"some error occured with the http post request");
			e1.printStackTrace();
		}

		// receiving the response
		HttpResponse response = null;
		HttpParams httpParams = new BasicHttpParams();

		KeyStore localTrustStore = null;
		try {
			localTrustStore = KeyStore.getInstance("BKS");
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InputStream in = context.getResources().openRawResource(
				R.raw.advanchip_trust_store);
		try {
			localTrustStore.load(in, "".toCharArray());
		} catch (NoSuchAlgorithmException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (CertificateException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		SSLSocketFactory sslSocketFactory = null;
		try {
			sslSocketFactory = new SSLSocketFactory(localTrustStore);
		} catch (KeyManagementException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnrecoverableKeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));

		ClientConnectionManager cm = new ThreadSafeClientConnManager(
				httpParams, schemeRegistry);

		HttpClient client = new DefaultHttpClient(cm, httpParams);

		try {
			response = client.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String output = null;
		try {
			output = EntityUtils.toString(response.getEntity());
			Log.i("fsd", output);

		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject responceJson = new JSONObject();
		if (output == null) {

			try {
				responceJson.put("Success", false);
				responceJson.put("Description",
						"The server didn't respond anything");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			JSONObject json = null;
			try {
				json = new JSONObject(output);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (json == null) {
				try {
					responceJson.put("Success", false);
					responceJson.put("Description",
							"Error! The server responded is not readable");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				try {
					if (json.has("error")) {

						responceJson.put("Success", false);
						responceJson.put("Description",
								json.getString("error_description"));

					} else if (json.has("email")) {
						responceJson.put("Success", true);
						responceJson.put("Description", "good");
						

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		return responceJson;

	}



	public static boolean refresh_token(Context context) {
		

		//curl -k https://advanchiplighting.com/oauth2/token -d 'grant_type=refresh_token' -d 'client_id=web_v1' 
		//-d 'client_secret=dHc1kSxJk9Hs6b7rLGpN52np5mnO8cEC' -d 'refresh_token=EuC-Tf1F4hftvbwBZIMqXxAjo65R1B4R�

		// Add your data

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("grant_type", "refresh_token"));
		nameValuePairs.add(new BasicNameValuePair("client_id", "android_v1"));
		nameValuePairs.add(new BasicNameValuePair("client_secret",
				GlobalConstants.CLIENT_SECRET));
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context.getApplicationContext());
		
		//int endOfSessionTime = sharedPrefs.getInt("expiresAt", 0);
		String refresh_token = sharedPrefs
				.getString("refresh_token", "default");
		nameValuePairs.add(new BasicNameValuePair("refresh_token",refresh_token));
		

		HttpPost request = new HttpPost(
				"https://www.advanchiplighting.com/oauth2/token");
		try {
			request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e1) {
			Log.e("http post error",
					"some error occured with the http post request");
			e1.printStackTrace();
		}

		// receiving the response
		HttpResponse response = null;
		HttpParams httpParams = new BasicHttpParams();

		KeyStore localTrustStore = null;
		try {
			localTrustStore = KeyStore.getInstance("BKS");
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InputStream in = context.getResources().openRawResource(
				R.raw.advanchip_trust_store);
		try {
			localTrustStore.load(in, "".toCharArray());
		} catch (NoSuchAlgorithmException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (CertificateException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		SSLSocketFactory sslSocketFactory = null;
		try {
			sslSocketFactory = new SSLSocketFactory(localTrustStore);
		} catch (KeyManagementException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnrecoverableKeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));

		ClientConnectionManager cm = new ThreadSafeClientConnManager(
				httpParams, schemeRegistry);

		HttpClient client = new DefaultHttpClient(cm, httpParams);

		try {
			response = client.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String output = null;
		try {
			output = EntityUtils.toString(response.getEntity());
			Log.i("fsd", output);

		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject responceJson = new JSONObject();
		if (output == null) {

			try {
				responceJson.put("Success", false);
				responceJson.put("Description",
						"The server didn't respond anything");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			JSONObject json = null;
			try {
				json = new JSONObject(output);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (json == null) {
				try {
					responceJson.put("Success", false);
					responceJson.put("Description",
							"Error! The server responded is not readable");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				try {
					if (json.has("error")) {

						responceJson.put("Success", false);
						responceJson.put("Description",
								json.getString("error_description"));

					} else if (json.has("access_token")) {
						responceJson.put("Success", true);
						responceJson.put("Description", "good");
						String access_token = json.getString("access_token");
						refresh_token = json.getString("refresh_token");
						String expires_in = json.getString("expires_in");
						long timeOfExpiration = json.getLong("expires_in")
								+ System.currentTimeMillis() / 1000;
						String token_type = json.getString("token_type");

						
						SharedPreferences.Editor editor = sharedPrefs.edit();
						editor.putString("access_token", access_token);
						editor.putString("refresh_token", refresh_token);
						editor.putString("expires_in", expires_in);
						editor.putString("token_type", token_type);
						editor.putLong("expiresAt", timeOfExpiration);

						editor.putBoolean("loggedIn", true).commit();
						return true;

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		

	
		return false;
	}





	public static void relogin(Context  context) {
		Intent intent = new Intent(context,
				LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
		
	}

	

}
