package com.advanchiplighting.remotelighting;

import org.holoeverywhere.app.Fragment;

import com.advanchiplighting.remotelighting.switchlisttab.AllSwitchFragment;


import android.support.v7.app.ActionBar;
import android.content.Context;
import android.os.Bundle;

public class TabBaseActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ActionBar bar = getSupportActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		/*
		// create tabs with icons
		ActionBar.Tab favoriteSwitch = bar.newTab().setIcon(R.drawable.ic_a);
		ActionBar.Tab recentSwitch = bar.newTab().setIcon(R.drawable.ic_a);
		ActionBar.Tab allSwitch = bar.newTab().setIcon(R.drawable.ic_a);
		ActionBar.Tab onSwitch = bar.newTab().setIcon(R.drawable.ic_a);
		ActionBar.Tab offSwitch = bar.newTab().setIcon(R.drawable.ic_a);*/
		// create tabs with texts
		ActionBar.Tab allSwitch = bar.newTab().setText("All");
		ActionBar.Tab onSwitch = bar.newTab().setText("On");
		ActionBar.Tab offSwitch = bar.newTab().setText("Off");
		
		
		// create fragments
		//Fragment favoriteFragment = new FavoriteSwitchFragment();
        //Fragment recentFragment = new RecentSwitchFragment();
		Fragment allFragment = new AllSwitchFragment("ALL");
        Fragment onFragment = new AllSwitchFragment("ON");
        Fragment offFragment = new AllSwitchFragment("OFF");
        
        
        // bind fragments to tabs
        //favoriteSwitch.setTabListener(new tabListener(favoriteFragment, getApplicationContext()));
        //recentSwitch.setTabListener(new tabListener(recentFragment, getApplicationContext()));
        allSwitch.setTabListener(new tabListener(allFragment, getApplicationContext()));
        onSwitch.setTabListener(new tabListener(onFragment, getApplicationContext()));
        offSwitch.setTabListener(new tabListener(offFragment, getApplicationContext()));
        
        
        // add tabs to the action bar
        //bar.addTab(favoriteSwitch);
        //bar.addTab(recentSwitch);
        bar.addTab(allSwitch);
        bar.addTab(onSwitch);
        bar.addTab(offSwitch);
        
	}

	private class tabListener implements ActionBar.TabListener {
		Fragment fragment;
		Context context;
		private tabListener (Fragment fragment, Context context){
			this.fragment=fragment;
			this.context=context;
		}
		
		@Override
		public void onTabSelected(android.support.v7.app.ActionBar.Tab tab,
				android.support.v4.app.FragmentTransaction ft) {
			ft.replace(R.id.content_frame, fragment);
		}

		@Override
		public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab,
				android.support.v4.app.FragmentTransaction ft) {
			ft.remove(fragment);
		}

		@Override
		public void onTabReselected(android.support.v7.app.ActionBar.Tab tab,
				android.support.v4.app.FragmentTransaction ft) {
			
		}	
	}
}