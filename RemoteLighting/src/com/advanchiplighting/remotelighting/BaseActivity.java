package com.advanchiplighting.remotelighting;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;
import org.holoeverywhere.widget.DrawerLayout;
import org.holoeverywhere.widget.ListView;

import com.advanchiplighting.remotelighting.account.LoginActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * This activity contains the drawer menu ui, which is inherited by other activities
 * @author John Lee
 *
 */
public class BaseActivity extends Activity {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    
    public static final int HDR_POS1 = 1;
    public static final int HDR_POS2 = 3;
    public static final int HDR_POS3 = 6;
    
    protected static boolean backBtnEnabled=true;

    private static final Integer LIST_HEADER = 0;
    private static final Integer LIST_ITEM = 1;
    
    public static final String[] mTitles = {"MENU",
		"LIGHTS",
		"LIGHTS",
		"ADMIN", 
		"MANAGE USERS", "MANAGE LIGHTS",
		"SETTINGS",
		"APP SETTINGS", "ABOUT", "HELP", "LOG OUT"};
    private static final int[] mImages = 
		{R.drawable.ic_action_menu, 
			0,
			R.drawable.ic_action_bulb,
			0,
			R.drawable.ic_action_manage_user, R.drawable.ic_action_gear,
			0,
			R.drawable.ic_action_settings, R.drawable.ic_action_about, R.drawable.ic_action_help, R.drawable.ic_action_logout
			};
    
   protected  SharedPreferences shared_preferences;
    protected SharedPreferences.Editor shared_preferences_editor;
     
	@Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	
    	shared_preferences = 
				PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    	shared_preferences_editor = shared_preferences.edit();
        
    	if (!userIsLoggedIn()) {
	        Intent intent = new Intent(getBaseContext(), LoginActivity.class);
            startActivity(intent);
            finish();
    	} else {
            setContentView(R.layout.activity_drawer);
            
            mTitle = mDrawerTitle = getTitle();
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (ListView) findViewById(R.id.left_drawer);
	    mDrawerList.setAdapter(new MyListAdapter(this));
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
            
            mDrawerToggle = new ActionBarDrawerToggle(
                    this,                  /* host Activity */
                    mDrawerLayout,         /* DrawerLayout object */
                    R.drawable.yellow_ic_navigation_drawer,  /* nav drawer image to replace 'Up' caret */
                    R.string.drawer_open,  /* "open drawer" description for accessibility */
                    R.string.drawer_close  /* "close drawer" description for accessibility */
                    ) {
                public void onDrawerClosed(View view) {
                	super.onDrawerClosed(view);
                    getSupportActionBar().setTitle(mTitle);
                }

                public void onDrawerOpened(View view) {
                	super.onDrawerOpened(view);
                    getSupportActionBar().setTitle(mDrawerTitle);
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
            
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);    		
    	}
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    private class MyListAdapter extends BaseAdapter {
        public MyListAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return mTitles.length;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            String headerText = getHeader(position);
            if(headerText != null) {

                View item = convertView;
                if(convertView == null || convertView.getTag() == LIST_ITEM) {

                    item = LayoutInflater.from(mContext).inflate(
                            R.layout.drawer_header, parent, false);
                    item.setTag(LIST_HEADER);

                }

                TextView headerTextView = (TextView)item.findViewById(R.id.drawer_list_header);
                headerTextView.setText(headerText);
                return item;
            }

            View item = convertView;
            if(convertView == null || convertView.getTag() == LIST_HEADER) {
                item = LayoutInflater.from(mContext).inflate(
                        R.layout.drawer_item, parent, false);
                item.setTag(LIST_ITEM);
            }

            TextView header = (TextView)item.findViewById(R.id.drawer_title);
            header.setText(mTitles[position % mTitles.length]);
            
            ImageView image = (ImageView)item.findViewById(R.id.drawer_logo);
            image.setImageResource(mImages[position % mImages.length]);
            

            return item;
        }

        private String getHeader(int position) {

            if(position == HDR_POS1  || position == HDR_POS2 || position == HDR_POS3) {
                return mTitles[position];
            }

            return null;
        }

        private final Context mContext;
    }    
    
    
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(android.widget.AdapterView<?> parent, View view,
				int position, long id) {
			mDrawerList.setItemChecked(position, true);
	        setTitle(mTitles[position]);
	        mDrawerLayout.closeDrawer(mDrawerList);
	        
	     // Do not use case number for header position (1,3,6)
	        Intent intent;
	        switch(position){
		        case 0:
		        	intent = new Intent(BaseActivity.this, MainActivity.class);
		        	startActivity(intent);
		        	break;
		        case 2:
		        	intent = new Intent(BaseActivity.this, LocationsActivity.class);
		        	startActivity(intent);
		        	break;
		        case 4:
		        	Log.i("base","clicked");
		        	break;
		        case 5:
		        	Log.i("base","clicked");
		        	break;
		        case 10:
		        	deleteUserCredentials();
		        	intent = new Intent(BaseActivity.this, MainActivity.class);
		        	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        	startActivity(intent);
		        	break;
		    } 
		}

		private void deleteUserCredentials() {
			
        	SharedPreferences sharedPrefs = 
	        		PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			SharedPreferences.Editor editor = sharedPrefs
					.edit();
			editor.clear();
			editor.commit();
			
		}
    }
    
    boolean userIsLoggedIn() {
        SharedPreferences sharedPrefs = 
        		PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return sharedPrefs.getBoolean("loggedIn", false);
	}
    
    int getUserId() {
        SharedPreferences sharedPrefs = 
        		PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return sharedPrefs.getInt("id", -1);
    }
}

