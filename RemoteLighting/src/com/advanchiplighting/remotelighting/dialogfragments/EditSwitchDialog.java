package com.advanchiplighting.remotelighting.dialogfragments;

import org.holoeverywhere.app.AlertDialog;

import com.advanchiplighting.remotelighting.R;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class EditSwitchDialog extends DialogFragment{

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View view = inflater.inflate(R.layout.dialog_edit_switch, null);
		
		final EditText nameInput = (EditText) view.findViewById(R.id.dialog_edit_switch_name);
		final EditText floorInput = (EditText) view.findViewById(R.id.dialog_edit_switch_floor);
		final EditText roomInput = (EditText) view.findViewById(R.id.dialog_edit_switch_room);
		
		//nameInput.setText(getArguments().getString("name"));
		//String id = getArguments().getString("id");
		//floorInput.setText(getArguments().getString("floor"));
		//roomInput.setText(getArguments().getShort("room"));
		
		builder.setTitle("Edit Switch");
		builder.setView(view);
		builder.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String nameString = nameInput.getText().toString();
				String floorString = floorInput.getText().toString();
				String roomString = roomInput.getText().toString();
				
				// TODO pass data to server
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		
		return builder.create();
	}
}
