package com.advanchiplighting.remotelighting.dialogfragments;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.DialogFragment;

import com.advanchiplighting.remotelighting.R;


import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class AddRoomDialog extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View view = inflater.inflate(R.layout.dialog_add_room, null);

		//getting all of the elements from the layout
		final Button cameraBtn = (Button)view.findViewById(R.id.dialog_add_room_camera);
		final EditText roomName = (EditText) view.findViewById(R.id.dialog_add_room_name);
		final Spinner spin = (Spinner)view.findViewById(R.id.dialog_add_room_floor_select);
		final EditText roomDesc = (EditText) view.findViewById(R.id.dialog_add_room_desc);


		//SET UP CAMER BUTTON
		cameraBtn.setOnClickListener(new View.OnClickListener(){//argument is an anonymous object
			@Override
			public void onClick(View v) {
				//ask for camera or gallery
				//implement 2 options.
			}
		});


		//SET UP SPINNER
		List<String> floors = new ArrayList<String>();
		floors.add("floor 1");
		floors.add("floor 2");
		//get list of floors from the web server


		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_item, floors); 

		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spin.setAdapter(dataAdapter);

		spin.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				Toast toast = new Toast(getActivity());
				toast.setText("No Floor Selected");
				toast.show();
			}
        });


		builder
		.setTitle("Add New Room")
		.setView(view)
		.setPositiveButton("Add", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//need to figure out how to pull the image (or if it is done when the camer stuff is chosen)

				//data validation (need to do for the spinner)
				//check name
				if (roomName.length() == 0) {
					roomName.setError("Required Field");
		        }
				//check spinner
				if(roomDesc.length()==0){
					roomDesc.setError("Required Field");
				}
				
				
				
				//pull strings from the name and edit text

				String name = roomName.getText().toString();
				String desc = roomDesc.getText().toString();
				String floor = spin.getSelectedItem().toString();
			}
		})
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			}
		});
		// Create the AlertDialog object and return it
		
		return builder.create();
	}
	
	 public static boolean isValid(EditText editText, String regex, String errMsg, boolean required) {
		 
	        String text = editText.getText().toString().trim();
	        // clearing the error, if it was previously set by some other values
	        editText.setError(null);
	 
	        // text required and editText is blank, so return false
	        if ( required && !hasText(editText) ) return false;
	 
	        // pattern doesn't match so returning false
	        if (required && !Pattern.matches(regex, text)) {
	            editText.setError(errMsg);
	            return false;
	        };
	 
	        return true;
	    }
	 
	 public static boolean hasText(EditText editText) {
		 
	        String text = editText.getText().toString().trim();
	        editText.setError(null);
	 
	        // length 0 means there is no text
	        if (text.length() == 0) {
	            editText.setError("Required Field");
	            return false;
	        }
	 
	        return true;
	    }
}
