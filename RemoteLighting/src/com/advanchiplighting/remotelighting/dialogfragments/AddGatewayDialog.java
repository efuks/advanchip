package com.advanchiplighting.remotelighting.dialogfragments;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.advanchiplighting.remotelighting.R;


public class AddGatewayDialog extends android.support.v4.app.DialogFragment {

	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	LayoutInflater inflater = getActivity().getLayoutInflater();

	View view = inflater.inflate(R.layout.dialog_add_gateway, null);

	//getting all of the elements from the layout
	final EditText gatewayID = (EditText)view.findViewById(R.id.dialog_gateway_id);
	final Button cancelBtn = (Button) view.findViewById(R.id.dialog_gateway_cancel);
	final Button okayBtn = (Button)view.findViewById(R.id.dialog_gateway_ok);

}
