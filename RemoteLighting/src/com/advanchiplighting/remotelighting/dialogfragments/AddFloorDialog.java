package com.advanchiplighting.remotelighting.dialogfragments;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.DialogFragment;

import com.advanchiplighting.remotelighting.R;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class AddFloorDialog extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View view = inflater.inflate(R.layout.dialog_add_floor, null);

		//getting all of the elements from the layout
		final Button cameraBtn = (Button)view.findViewById(R.id.dialog_add_floor_camera);
		final EditText floorName = (EditText) view.findViewById(R.id.dialog_add_floor_name);
		final EditText floorDesc = (EditText) view.findViewById(R.id.dialog_add_floor_desc);


		//SET UP CAMER BUTTON
		cameraBtn.setOnClickListener(new View.OnClickListener(){//argument is an anonymous object
			@Override
			public void onClick(View v) {
				//ask for camera or gallery
				//implement 2 options.
			}
		});


		builder
		.setTitle("Add New Floor")
		.setView(view)
		.setPositiveButton("Add", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//need to figure out how to pull the image (or if it is done when the camer stuff is chosen)

				//data validation (need to do for the spinner)
				//check name
				if (floorName.length() == 0) {
					floorName.setError("Required Field");
		        }
				//check spinner
				if(floorDesc.length()==0){
					floorDesc.setError("Required Field");
				}
				
				
				
				//pull strings from the name and edit text

				String name = floorName.getText().toString();
				String desc = floorDesc.getText().toString();
			}
		})
		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			}
		});
		// Create the AlertDialog object and return it
		
		return builder.create();
	}
	
	 public static boolean isValid(EditText editText, String regex, String errMsg, boolean required) {
		 
	        String text = editText.getText().toString().trim();
	        // clearing the error, if it was previously set by some other values
	        editText.setError(null);
	 
	        // text required and editText is blank, so return false
	        if ( required && !hasText(editText) ) return false;
	 
	        // pattern doesn't match so returning false
	        if (required && !Pattern.matches(regex, text)) {
	            editText.setError(errMsg);
	            return false;
	        };
	 
	        return true;
	    }
	 
	 public static boolean hasText(EditText editText) {
		 
	        String text = editText.getText().toString().trim();
	        editText.setError(null);
	 
	        // length 0 means there is no text
	        if (text.length() == 0) {
	            editText.setError("Required Field");
	            return false;
	        }
	 
	        return true;
	    }
}
