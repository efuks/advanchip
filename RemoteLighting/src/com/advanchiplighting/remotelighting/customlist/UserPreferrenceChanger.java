package com.advanchiplighting.remotelighting.customlist;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;

import com.advanchiplighting.remotelighting.R;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class UserPreferrenceChanger extends AsyncTask<Void, Void, String[]> {
	String gatewayID;
	String userID;
	Context context;

	public UserPreferrenceChanger(String gatewayID, String userID,
			Context context) {
		this.gatewayID = gatewayID;
		this.userID = userID;
		this.context = context;
		
		this.execute();

	}

	@Override
	protected String[] doInBackground(Void... arg0) {

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("level", "1"));
		HttpPut request = new HttpPut(
				"https://www.advanchiplighting.com/api/v1/gateways/535451718b9499c37df2afd0/permissions/533fbcdbfad8c4753b5f96e0");

		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context
						.getApplicationContext());
		String access_token = sharedPrefs.getString("access_token",
				"default");
		request.setHeader("Authorization", "Bearer " + access_token);
		try {
			request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e1) {
			Log.e("http post error",
					"some error occured with the http post request");
			e1.printStackTrace();
		}

		// receiving the response
		HttpResponse response = null;
		HttpParams httpParams = new BasicHttpParams();

		KeyStore localTrustStore = null;
		try {
			localTrustStore = KeyStore.getInstance("BKS");
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InputStream in = context.getResources().openRawResource(
				R.raw.advanchip_trust_store);
		try {
			localTrustStore.load(in, "".toCharArray());
		} catch (NoSuchAlgorithmException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (CertificateException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		SSLSocketFactory sslSocketFactory = null;
		try {
			sslSocketFactory = new SSLSocketFactory(localTrustStore);
		} catch (KeyManagementException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnrecoverableKeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));

		ClientConnectionManager cm = new ThreadSafeClientConnManager(
				httpParams, schemeRegistry);

		HttpClient client = new DefaultHttpClient(cm, httpParams);

		try {
			response = client.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}