package com.advanchiplighting.remotelighting.customlist;

public class SwitchItemComponent {
	private String gatewayId;
	private int drawableImage;
	private String roomName;
	private String floorName;
	private boolean switchStatus;
	
	
	
	public SwitchItemComponent() {
		// TODO Auto-generated constructor stub
	}

	public String getGatewayId() {
		return gatewayId;
	}
	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public void setDrawableImage(int drawableImage) {
		this.drawableImage = drawableImage;
	}

	

	public void setSwitchStatus(boolean switchStatus) {
		this.switchStatus = switchStatus;
	}

	public int getDrawableImage() {
		return drawableImage;
	}
	
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getFloorName() {
		return floorName;
	}

	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}

	public boolean isSwitchStatus() {
		return switchStatus;
	}
}
