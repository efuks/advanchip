package com.advanchiplighting.remotelighting.customlist;

import java.util.ArrayList;

import com.advanchiplighting.remotelighting.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LocationAdapter2 extends BaseAdapter {
	Context context;
	int layout;
	ArrayList<LocationItem2> list;
	LayoutInflater inflater;
	
	public LocationAdapter2 (Context context, int layout, ArrayList<LocationItem2> list){
		this.context=context;
		this.layout=layout;
		this.list=list;
		inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int index=position;
		if (convertView==null){
			convertView=inflater.inflate(layout, parent, false);
		}
		
		TextView name = (TextView) convertView.findViewById(R.id.item_onestring);
		name.setText(list.get(position).getName());
		
		return convertView;
	}
	
}
