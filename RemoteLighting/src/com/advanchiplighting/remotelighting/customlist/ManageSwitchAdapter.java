package com.advanchiplighting.remotelighting.customlist;

import java.util.ArrayList;


import com.advanchiplighting.remotelighting.R;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ManageSwitchAdapter extends BaseAdapter{
	Context context;
	int layout;
	ArrayList<ManageSwitchItem> list;
	LayoutInflater inflater;
	
	public ManageSwitchAdapter(Context context, int layout, ArrayList<ManageSwitchItem> list){
		this.context=context;
		this.layout=layout;
		this.list=list;
		inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int index=position;
		if (convertView==null){
			convertView=inflater.inflate(layout, parent, false);
		}
		
		ImageView image=(ImageView) convertView.findViewById(R.id.m_switch_image);
		image.setImageResource(list.get(index).getDrawableImage());
		
		TextView text=(TextView) convertView.findViewById(R.id.m_switch_name);
		text.setText(list.get(index).getSwitchName());
		
		TextView location=(TextView)convertView.findViewById(R.id.m_switch_location);
		location.setText(list.get(index).getGatewayId()+" | "+list.get(index).getFloor()+" | "+list.get(index).getRoom());
		
		Button button=(Button)convertView.findViewById(R.id.m_switch_edit);
		button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Edit Switch Dialog here
				final Dialog editSwitch = new Dialog(context);
				editSwitch.setContentView(R.layout.dialog_edit_switch);
				editSwitch.setTitle("Edit Switch");
				final EditText name = (EditText) editSwitch.findViewById(R.id.dialog_edit_switch_name);
				final EditText floor = (EditText) editSwitch.findViewById(R.id.dialog_edit_switch_floor);
				final EditText room = (EditText) editSwitch.findViewById(R.id.dialog_edit_switch_room);
				
				name.setText(list.get(index).getSwitchName());
				floor.setText(list.get(index).getFloor());
				room.setText(list.get(index).getRoom());
				
				Button ok = (Button) editSwitch.findViewById(R.id.dialog_edit_switch_ok);
				Button cancel = (Button) editSwitch.findViewById(R.id.dialog_edit_switch_cancel);
				ok.setOnClickListener(new Button.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						editSwitch.dismiss();
					}
				});
				cancel.setOnClickListener(new Button.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						editSwitch.dismiss();
					}
				});
				
				editSwitch.show();
			}
		});
		
		return convertView;
	}

}
