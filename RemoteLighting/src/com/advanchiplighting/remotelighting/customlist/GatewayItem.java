package com.advanchiplighting.remotelighting.customlist;

public class GatewayItem {
	private String id;
	private String name;
	
	public GatewayItem() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
