package com.advanchiplighting.remotelighting.customlist;

public class LocationItem2 {
	private String name;
	private String id;
	
	public LocationItem2() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
