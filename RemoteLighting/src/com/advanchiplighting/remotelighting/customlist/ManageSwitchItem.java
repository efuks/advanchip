package com.advanchiplighting.remotelighting.customlist;

public class ManageSwitchItem {
	private int drawableImage;
	private String switchName;
	private String gatewayId;
	private String floor;
	private String room;
	private String switchId;
	
	

	public ManageSwitchItem(String switchName, String gatewayId, String floor, String room){
		this.switchName = switchName;
		this.gatewayId = gatewayId;
		this.floor = floor;
		this.room = room;
	}

	public void setSwitchName(String switchName) {
		this.switchName = switchName;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public ManageSwitchItem() {
		// TODO Auto-generated constructor stub
	}

	public int getDrawableImage() {
		return drawableImage;
	}

	public String getSwitchName() {
		return switchName;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public String getFloor() {
		return floor;
	}

	public String getRoom() {
		return room;
	}
	public String getSwitchId() {
		return switchId;
	}

	public void setSwitchId(String switchId) {
		this.switchId = switchId;
	}
}
