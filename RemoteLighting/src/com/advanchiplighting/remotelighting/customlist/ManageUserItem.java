package com.advanchiplighting.remotelighting.customlist;

public class ManageUserItem {
	private String userName;
	private int permission;
	private String id;
	private String gatewayID;
	
	public String getGatewayID() {
		return gatewayID;
	}

	public void setGatewayID(String gatewayID) {
		this.gatewayID = gatewayID;
	}

	public ManageUserItem(String userName, int permission){
		this.userName = userName;
		this.permission = permission;
	}
	
	public ManageUserItem() {
		// TODO Auto-generated constructor stub
	}

	public String getUserName() {
		return userName;
	}
	public String getID() {
		return id;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getPermission() {
		
		return permission;
	}
	public void setPermission(int permission) {
		this.permission = permission;
	}
	public void setID(String id) {
		this.id = id;
	}

}
