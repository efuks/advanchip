package com.advanchiplighting.remotelighting.customlist;

public class SwitchItem {
	private String switchId;
	private String gatewayId;
	private int drawableImage;
	private String switchName;
	private boolean switchStatus;	
/*	public SwitchItem(int drawableImage, String switchName, boolean switchStatus){
		this.drawableImage = drawableImage;
		this.switchName = switchName;
		this.switchStatus = switchStatus;
	}*/
	
	public SwitchItem(String switchId, String gatewayId, String switchName, boolean switchStatus){
		this.switchId = switchId;
		this.gatewayId = gatewayId;
		this.switchName = switchName;
		this.switchStatus = switchStatus;
	}

	public String getSwitchId() {
		return switchId;
	}

	public int getDrawableImage() {
		return drawableImage;
	}

	public String getSwitchName() {
		return switchName;
	}

	public boolean isSwitchStatus() {
		return switchStatus;
	}
	
	public void setSwitchStatus(boolean status){
		this.switchStatus=status;
	}

	public String getGatewayId() {
		return gatewayId;
	}
}
