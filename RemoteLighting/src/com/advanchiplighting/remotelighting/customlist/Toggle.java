package com.advanchiplighting.remotelighting.customlist;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;

import com.advanchiplighting.remotelighting.GlobalConstants;

public class Toggle extends AsyncTask<Void, Void, String[]> {
	String id;
	public Toggle (String id){
		this.id = id;
	}
	@Override
	protected String[] doInBackground(Void... arg0) {

		String request = "";

		Log.i("request: ", request);
		String requestAddress = request;

		StringBuilder url = new StringBuilder();
		url.append(requestAddress);
		HttpGet get = new HttpGet(url.toString());
		HttpClient client = new DefaultHttpClient();
		HttpUriRequest req = get;
		HttpResponse resp = null;
		try {
			resp = client.execute(req);
		} catch (Exception e) {
			e.printStackTrace();
		}

		HttpEntity httpEntity = resp.getEntity();

		String output = null;
		try {
			output = EntityUtils.toString(httpEntity);
			Log.i("response", output);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}