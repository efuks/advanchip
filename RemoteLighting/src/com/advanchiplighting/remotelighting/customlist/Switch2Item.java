package com.advanchiplighting.remotelighting.customlist;

public class Switch2Item {
	private int drawableImage;
	private String switchName;
	private boolean switchStatus;
	private String switchLocation;
	
	public Switch2Item (int drawableImage, String switchName, boolean switchStatus, String switchLocation){
		this.drawableImage = drawableImage;
		this.switchName = switchName;
		this.switchStatus = switchStatus;
		this.switchLocation = switchLocation;
	}

	public int getDrawableImage() {
		return drawableImage;
	}

	public String getSwitchName() {
		return switchName;
	}

	public boolean isSwitchStatus() {
		return switchStatus;
	}

	public String getSwitchLocation() {
		return switchLocation;
	}
}
