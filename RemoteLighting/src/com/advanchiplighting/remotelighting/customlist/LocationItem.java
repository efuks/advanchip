package com.advanchiplighting.remotelighting.customlist;

public class LocationItem {
	private int drawableImage;
	private String locationName;
	private int indicatorImage;
	
	public LocationItem(int drawableImage, String locationName, int indicatorImage) {
		this.drawableImage = drawableImage;
		this.locationName = locationName;
		this.indicatorImage = indicatorImage;
	}

	public int getDrawableImage() {
		return drawableImage;
	}

	public String getLocationName() {
		return locationName;
	}
	
	public int getIndicatorImage() {
		return indicatorImage;
	}
}
