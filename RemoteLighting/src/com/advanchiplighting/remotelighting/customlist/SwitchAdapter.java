package com.advanchiplighting.remotelighting.customlist;

import https.HTTPPut;

import java.util.ArrayList;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.Switch;
import org.json.JSONException;
import org.json.JSONObject;

import com.advanchiplighting.remotelighting.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

public class SwitchAdapter extends BaseAdapter {
	Context context;
	int layout;
	ArrayList<SwitchItem> list;
	LayoutInflater inflater;
	private String gatewayID;
	private String lightID;

	public SwitchAdapter(Context context, int layout, ArrayList<SwitchItem> list) {
		this.context = context;
		this.layout = layout;
		this.list = list;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void receiveData(String gatewayId, String lightId) {
		this.gatewayID = gatewayId;
		this.lightID = lightId;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final int index = position;
		if (convertView == null) {
			convertView = inflater.inflate(layout, parent, false);
		}
		/*
		 * ImageView image=(ImageView)
		 * convertView.findViewById(R.id.switch_image);
		 * image.setImageResource(list.get(index).getDrawableImage());
		 */
		TextView text = (TextView) convertView.findViewById(R.id.switch_name);
		text.setText(list.get(index).getSwitchName());
		
		final String gateway=list.get(index).getGatewayId();
		final String light=list.get(index).getSwitchId(); 

		Switch toggle = (Switch) convertView.findViewById(R.id.switch_switch);
		toggle.setChecked(list.get(index).isSwitchStatus());
		toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				
				
					Activity activity = (Activity) context;
					Log.i("Toggled", "the light suppose to be on");
					JSONObject request = new JSONObject();

					String value;
					if (list.get(index).isSwitchStatus()){
						value="0";
					}else{
						value="1";
					}
					
					list.get(index).setSwitchStatus(!list.get(index).isSwitchStatus());
					try {
						request.put("instruction", "toggleLight");
						request.put("gatewayID", gateway);
						request.put("gateID", light);
						request.put("state",value );
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					ChangeLightRequest put = new ChangeLightRequest(context, request);
					put.execute();
				
			}
		});

		return convertView;
	}

}