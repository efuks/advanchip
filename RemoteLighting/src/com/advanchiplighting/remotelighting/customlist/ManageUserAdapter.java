package com.advanchiplighting.remotelighting.customlist;

import java.util.ArrayList;

import org.holoeverywhere.app.Dialog;

import com.advanchiplighting.remotelighting.R;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class ManageUserAdapter extends BaseAdapter {
	Context context;
	int layout;
	ArrayList<ManageUserItem> list;
	LayoutInflater inflater;
	
	public ManageUserAdapter (Context context, int layout, ArrayList<ManageUserItem> list){
		this.context=context;
		this.layout=layout;
		this.list=list;
		inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final int index=position;
		if (convertView==null){
			convertView=inflater.inflate(layout, parent, false);
		}
		
		TextView name = (TextView) convertView.findViewById(R.id.manage_user_name);
		name.setText(list.get(index).getUserName());
		
		TextView permission = (TextView) convertView.findViewById(R.id.manage_user_permission);
		permission.setText(Integer.toString(list.get(index).getPermission()));
		
		final ImageView menu = (ImageView) convertView.findViewById(R.id.manage_user_menu);
		menu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PopupMenu popupMenu = new PopupMenu(context, menu);
				
				popupMenu.getMenu().add(Menu.NONE, 1, Menu.NONE, "Permission");
				popupMenu.getMenu().add(Menu.NONE, 2, Menu.NONE, "Delete");
				popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						Log.i("popup_menu",item.getItemId()+" selected");
						switch(item.getItemId()){
						
						case 1:
							// modify name
							final Dialog editUser = new Dialog(context);
							boolean isAdmin=false;
							
							editUser.setContentView(R.layout.dialog_edit_user);
							final ManageUserItem user	=(ManageUserItem)getItem(position);
							if (user.getPermission()==2){
								isAdmin=true;
							
							}
							editUser.setTitle("Edit "+user.getUserName());
							
							final CheckBox adminCB=(CheckBox)editUser.findViewById(R.id.dialog_edit_user_checkbox);
							adminCB.setChecked(isAdmin);
							TextView userId = (TextView) editUser.findViewById(R.id.dialog_edit_user_id);
							// no user id? (email address)
							userId.setText(user.getUserName());
							Button ok = (Button) editUser.findViewById(R.id.dialog_edit_user_ok);
							Button cancel = (Button) editUser.findViewById(R.id.dialog_edit_user_cancel);
							
							ok.setOnClickListener(new Button.OnClickListener() {
								
								@Override
								public void onClick(View v) {
								String level;
								if (adminCB.isChecked()){
									level="2";
								}else{
									level="1";
								}
									ChangePermissionRequest tog =new ChangePermissionRequest(context, level, user.getGatewayID(), user.getID(),"changePermissions");
									tog.execute();
								
								
									editUser.dismiss();
								}
							});
							cancel.setOnClickListener(new Button.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									editUser.dismiss();
								}
							});
							
							
							
							editUser.show();
							break;
						case 2:
							final ManageUserItem userToDelete	=(ManageUserItem)getItem(position);
							ChangePermissionRequest tog =new ChangePermissionRequest(context, null, userToDelete.getGatewayID(), userToDelete.getID(),"deleteUserFromGateway");
							tog.execute();
							break;
						}
						return false;
					}
				});
				popupMenu.show();
			}
		});
		
		return convertView;
	}

}
