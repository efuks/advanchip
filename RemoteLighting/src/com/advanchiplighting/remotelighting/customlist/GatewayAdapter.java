package com.advanchiplighting.remotelighting.customlist;

import java.util.ArrayList;

import com.advanchiplighting.remotelighting.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class GatewayAdapter extends BaseAdapter {
	Context context;
	int layout;
	ArrayList<GatewayItem> list;
	LayoutInflater inflater;
	
	public GatewayAdapter (Context context, int layout, ArrayList<GatewayItem> list) {
		this.context = context;
		this.layout = layout;
		this.list = list;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView text = (TextView) convertView.findViewById(R.id.item_onestring);
		text.setText(list.get(position).getName());
		return convertView;
	}
}
