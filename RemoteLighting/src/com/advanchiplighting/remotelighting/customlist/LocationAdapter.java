package com.advanchiplighting.remotelighting.customlist;

import java.util.ArrayList;
import org.holoeverywhere.widget.Switch;
import com.advanchiplighting.remotelighting.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

public class LocationAdapter extends BaseAdapter{
	Context context;
	int layout;
	ArrayList<SwitchItem> list;
	LayoutInflater inflater;
	
	public LocationAdapter(Context context, int layout, ArrayList<SwitchItem> list){
		this.context=context;
		this.layout=layout;
		this.list=list;
		inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final int index=position;
		if (convertView==null){
			convertView=inflater.inflate(layout, parent, false);
		}
		/*
		ImageView image=(ImageView) convertView.findViewById(R.id.switch_image);
		image.setImageResource(list.get(index).getDrawableImage());
		*/
		TextView text=(TextView) convertView.findViewById(R.id.switch_name);
		text.setText(list.get(index).getSwitchName());
		
		Switch toggle=(Switch)convertView.findViewById(R.id.switch_switch);
		toggle.setChecked(list.get(index).isSwitchStatus());
		toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		    	
		    }
		});
		
		return convertView;
	}
}
