package com.advanchiplighting.remotelighting.customlist;

import java.util.ArrayList;

import org.holoeverywhere.widget.Switch;

import com.advanchiplighting.remotelighting.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Switch2Adapter extends BaseAdapter {
	Context context;
	int layout;
	ArrayList<Switch2Item> list;
	LayoutInflater inflater;
	public Switch2Adapter(Context context, int layout, ArrayList<Switch2Item> list){
		this.context=context;
		this.layout=layout;
		this.list=list;
		inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	@Override
	public int getCount() {
		return list.size();
	}
	@Override
	public Object getItem(int position) {
		return list.get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int index=position;
		if (convertView==null){
			convertView=inflater.inflate(layout, parent, false);
		}
		
		ImageView image=(ImageView) convertView.findViewById(R.id.sl_image);
		image.setImageResource(list.get(index).getDrawableImage());
		
		TextView text=(TextView) convertView.findViewById(R.id.sl_name);
		text.setText(list.get(index).getSwitchName());
		
		Switch toggle=(Switch)convertView.findViewById(R.id.sl_switch);
		// TODO switch handler
		
		return convertView;
	}
}
