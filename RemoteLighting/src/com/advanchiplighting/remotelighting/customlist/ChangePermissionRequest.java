package com.advanchiplighting.remotelighting.customlist;

import https.HTTPDelete;
import https.HTTPPut;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

public class ChangePermissionRequest extends AsyncTask<Void, Void, String[]> {
	Context context;
	String gatewayID, userID, permission, instruction;

	public ChangePermissionRequest(Context id, String permission,
			String gatewayID, String userID, String instruction) {
		this.context = id;
		this.permission = permission;
		this.gatewayID = gatewayID;
		this.userID = userID;
		this.instruction = instruction;

	}

	@Override
	protected String[] doInBackground(Void... param) {

		if (instruction.equals("changePermissions")) {
			JSONObject json = new JSONObject();

			try {
				json.put("userID", userID);
				json.put("gatewayID", gatewayID);
				json.put("instruction", instruction);
				json.put("permissions", permission);
			} catch (JSONException e) {

				e.printStackTrace();
				return null;
			}

			HTTPPut put = new HTTPPut(context, json);
			put.runHTTPPut();
		}else if (instruction.equals("deleteUserFromGateway")) {
			JSONObject json = new JSONObject();

			try {
				json.put("userID", userID);
				json.put("gatewayID", gatewayID);
				json.put("instruction", instruction);
			} catch (JSONException e) {

				e.printStackTrace();
				return null;
			}

			HTTPDelete delete = new HTTPDelete(context, json);
			delete.runHTTPPut();
		}

		return null;
	}

}