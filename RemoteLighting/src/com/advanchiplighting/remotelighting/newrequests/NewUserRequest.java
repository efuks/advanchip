package com.advanchiplighting.remotelighting.newrequests;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.advanchiplighting.remotelighting.R;

public class NewUserRequest {
	private static Context context;
	private static String gateway;
	private static String email;
	private static String output;

	public NewUserRequest(String gatewayID, Context id, String email) {
		this.context = id;
		this.gateway = gatewayID;
		this.email = email;
	}

	public JSONObject addTheNewUser() {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("email", email));
		nameValuePairs.add(new BasicNameValuePair("level", "1"));

		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context.getApplicationContext());
		String access_token = sharedPrefs.getString("access_token", "default");
		

		HttpPost request = new HttpPost(
				"https://www.advanchiplighting.com/api/v1/gateways/" + gateway
						+ "/permissions");
		
		request.setHeader("Authorization", "Bearer " + access_token);
		try {
			request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e1) {
			Log.e("http post error",
					"some error occured with the http post request");
			e1.printStackTrace();
		}

		// receiving the response
		HttpResponse response = null;
		HttpParams httpParams = new BasicHttpParams();

		KeyStore localTrustStore = null;
		try {
			localTrustStore = KeyStore.getInstance("BKS");
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InputStream in = context.getResources().openRawResource(
				R.raw.advanchip_trust_store);
		try {
			localTrustStore.load(in, "".toCharArray());
		} catch (NoSuchAlgorithmException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (CertificateException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		SSLSocketFactory sslSocketFactory = null;
		try {
			sslSocketFactory = new SSLSocketFactory(localTrustStore);
		} catch (KeyManagementException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnrecoverableKeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));

		ClientConnectionManager cm = new ThreadSafeClientConnManager(
				httpParams, schemeRegistry);

		HttpClient client = new DefaultHttpClient(cm, httpParams);

		try {
			response = client.execute(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String output = null;
		try {
			output = EntityUtils.toString(response.getEntity());
			Log.i("fsd", output);

		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONObject responceJson = new JSONObject();
		if (output == null) {

			try {
				responceJson.put("Success", false);
				responceJson.put("Description",
						"The server didn't respond anything");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (output.equals("User not found")) {
			try {
				responceJson.put("Success", false);
				responceJson.put("Description", "User was not found");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (output
				.equals("User already has permissions for this gateway")) {
			try {
				responceJson.put("Success", false);
				responceJson.put("Description",
						"User already has permissions for this gateway");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (output.equals("Unauthorized")) {

			try {
				responceJson.put("Success", false);
				responceJson.put("Description", "Unauthorized");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {

			JSONObject json = null;
			try {
				json = new JSONObject(output);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (json == null) {
				try {
					responceJson.put("Success", false);
					responceJson.put("Description",
							"Error! The server responded is not readable");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				try {
					if (json.has("error")) {

						responceJson.put("Success", false);
						responceJson.put("Description",
								json.getString("error_description"));

					} else if (json.has("userId")) {
						responceJson.put("Success", true);
						responceJson.put("Description", "good");

						responceJson.put("name", json.get("userDisplayName"));

					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		return responceJson;

	}
}