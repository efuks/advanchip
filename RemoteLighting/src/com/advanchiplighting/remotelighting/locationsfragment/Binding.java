package com.advanchiplighting.remotelighting.locationsfragment;

import https.HTTPPostRequest;

import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

public class Binding extends AsyncTask<Void, Void, JSONObject> {

	Context context;
	JSONObject json;

	public Binding(Context context, JSONObject json) {
		this.context = context;
		this.json = json;
	}

	@Override
	protected JSONObject doInBackground(Void... params) {

		HTTPPostRequest post=new HTTPPostRequest(context, json);
		post.runHTTPPost();
		
		return null;
	}

}