package com.advanchiplighting.remotelighting.locationsfragment;

import android.os.Bundle;
import android.util.Log;
import com.advanchiplighting.remotelighting.TabBaseActivity;

public class SwitchesActivity extends TabBaseActivity {

	static String requestedFloor;
	static String requestedGateway;
	static String requestedRoom;
	String requestedLightId;

	@Override
	public void onCreate(
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = this.getIntent().getExtras();
		requestedFloor = bundle.getString("floor");
		requestedGateway = bundle.getString("gatewayID");
		requestedRoom = bundle.getString("room");
		Log.i("requested floor", requestedFloor);
	
	}
	
	public String getGateway(){
		return requestedGateway;
	}

	 public String getRequestedFloor(){
		return requestedFloor;
	}
	
	 public String getRequestedRoom(){
		return requestedRoom;
	}
	
	
}
