package com.advanchiplighting.remotelighting.locationsfragment;

import https.HTTPGetData;

import java.util.ArrayList;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.account.AccountControl;
import com.advanchiplighting.remotelighting.customlist.ManageSwitchAdapter;
import com.advanchiplighting.remotelighting.customlist.ManageSwitchItem;

public class SwitchListFragment extends Fragment {

	private static final Integer USER_ITEM = 0;
	private static final Integer ADD_USER_ITEM = 1;
	private String requestedGateway;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.list_one_button, container,
				false);
		
		Button button = (Button) view.findViewById(R.id.one_button);
		button.setText("Add Switch");
		button.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO add switch dialog
				final Dialog addSwitch = new Dialog(getActivity());
				addSwitch.setContentView(R.layout.dialog_bind_switch);
				addSwitch.setTitle("Add Switch");
				final EditText switchInput = (EditText) addSwitch.findViewById(R.id.dialog_bind_switch_id);
				final EditText switchName = (EditText) addSwitch.findViewById(R.id.dialog_bind_switch_name);
				Button ok = (Button) addSwitch.findViewById(R.id.dialog_bind_switch_ok);
				Button cancel = (Button) addSwitch.findViewById(R.id.dialog_bind_switch_cancel);
				
				ok.setOnClickListener(new Button.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						JSONObject data= new JSONObject();
						
						try {
							data.put("gatewayID",requestedGateway);
							data.put("instruction","bindSwitch");
							data.put("serialNumber", switchInput.getText().toString());
							data.put("name", switchName.getText().toString());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Binding bind=new Binding(getActivity(),data);
						bind.execute();
						addSwitch.dismiss();
					}
				});
				cancel.setOnClickListener(new Button.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						addSwitch.dismiss();
					}
				});
				
				addSwitch.show();
			}
		});
		
		getActivity().setTitle("switches");
		Bundle bundle = this.getArguments();
		requestedGateway = bundle.getString("gatewayID");
		
		new FetchSwitchTaask().execute();

		return view;

	}

	

	private class FetchSwitchTaask extends AsyncTask<Void, Void, ArrayList<ManageSwitchItem>> {
		private SwitchListFragment fragment = SwitchListFragment.this;

		@Override
		protected void onPreExecute() {
			//showProgressBar();
		}

		@Override
		protected ArrayList<ManageSwitchItem> doInBackground(Void... params) {
			

			String output;
			JSONArray json = null;
			
			
			JSONObject requestJson = new JSONObject();

			try {
				requestJson.put("instruction", "gateway");
				requestJson.put("gatewayID", requestedGateway);
			} catch (JSONException e) {
				
				e.printStackTrace();
				new ArrayList<ManageSwitchItem>();
			}
			output=HTTPGetData.fetchGatewayData(requestJson,getActivity());
			if (output.equals("relogin")){
				return null;
			}
			
			try {
				json=new JSONObject(output).getJSONArray("gates");
			} catch (JSONException e1) {
				
				e1.printStackTrace();
				new ArrayList<ManageSwitchItem>();
			}
			
			 
			ArrayList<ManageSwitchItem> switches = new ArrayList<ManageSwitchItem>();
			for (int i = 0; i < json.length(); i++) {
				try {

					
					ManageSwitchItem sw=new ManageSwitchItem();
					
					
					sw.setGatewayId(requestedGateway);
					sw.setSwitchName((String) json.getJSONObject(i).get("name"));
					sw.setSwitchId((String) json.getJSONObject(i).get("_id"));
					switches.add(sw);
				} catch (JSONException e) {
					Log.e("json", "error reading json");
					e.printStackTrace();
				}
			}
			Log.i("fetching", "fetching is complete");
			return switches;
		
			
			
			
			
		}

		@Override
		protected void onPostExecute(ArrayList<ManageSwitchItem> switches) {
			//hideProgressBar();
			if (switches == null) {
				AccountControl.relogin(getActivity());
			}
			
			setSwitchesList(switches);
		}

		

		/*private void showProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (!progressBar.isShown()) {
				progressBar.setVisibility(View.VISIBLE);
			}
		}

		private void hideProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (progressBar.isShown()) {
				progressBar.setVisibility(View.GONE);
			}
		}
		
		*/
		
		private void setSwitchesList(ArrayList<ManageSwitchItem>  switches) {
			ListView listview = (ListView) fragment.getView().findViewById(R.id.one_button_listview);
		ManageSwitchAdapter adapter = new ManageSwitchAdapter(getActivity(), R.layout.item_manage_switch, switches);

		listview.setAdapter(adapter);
			
		}
	}
}
