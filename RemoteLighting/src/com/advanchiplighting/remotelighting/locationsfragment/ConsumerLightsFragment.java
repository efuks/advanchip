package com.advanchiplighting.remotelighting.locationsfragment;

import java.util.ArrayList;

import https.HTTPGetData;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.account.AccountControl;
import com.advanchiplighting.remotelighting.customlist.LocationAdapter2;
import com.advanchiplighting.remotelighting.customlist.LocationItem2;

public class ConsumerLightsFragment extends Fragment {

	String requestedFloor,instruction,requestedGateway;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.plain_basic_list, container,
				false);
		// Log.i("info from the gateway",savedInstanceState.getString("gatewayID"));
		Bundle bundle = this.getArguments();
		instruction = bundle.getString("instruction");

		requestedFloor = bundle.getString("floorID");

		requestedGateway = bundle.getString("gatewayID");
		

		if (instruction.equals("gateways")) {
			getActivity().setTitle("Your Gateways");
		} else if (instruction.equals("floors")) {
			getActivity().setTitle(requestedGateway);
		} else if (instruction.equals("rooms")) {
			getActivity().setTitle(requestedFloor);
		}
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		new FetchGatewaysTask().execute();
	}

	private class FetchGatewaysTask extends
			AsyncTask<Void, Void, ArrayList<LocationItem2>> {
		private ConsumerLightsFragment fragment = ConsumerLightsFragment.this;

		@Override
		protected void onPreExecute() {
			showProgressBar();
		}

		@Override
		protected ArrayList<LocationItem2> doInBackground(Void... params) {

			String output;
			JSONArray json = null;

			JSONObject requestJson = new JSONObject();

			if (instruction.equals("rooms")) {
				try {
					requestJson.put("instruction", "rooms");
					requestJson.put("gatewayID", requestedGateway);
					
					requestJson.put("floorID", requestedFloor);
				} catch (JSONException e) {

					e.printStackTrace();
					return new ArrayList<LocationItem2>();
				}
			} else if (instruction.equals("floors")) {
				try {
					requestJson.put("instruction", "floors");
					requestJson.put("gatewayID", requestedGateway);
				} catch (JSONException e) {

					e.printStackTrace();
					return new ArrayList<LocationItem2>();
				}
			} else {
				try {
					requestJson.put("instruction", "gateways");

				} catch (JSONException e) {

					e.printStackTrace();
					return new ArrayList<LocationItem2>();
				}
			}

			output = HTTPGetData.fetchGatewayData(requestJson, getActivity());
			try {
				json = new JSONArray(output);
			} catch (JSONException e1) {

				e1.printStackTrace();
				return new ArrayList<LocationItem2>();
			}
			JSONObject tree = null;

			
			
			ArrayList<LocationItem2> rooms = new ArrayList<LocationItem2>();
			
			for (int i = 0; i < json.length(); i++) {
				try {

					String name = (String) json.getJSONObject(i).get("name");

					String id = (String) json.getJSONObject(i).get("_id");
					LocationItem2 item = new LocationItem2();
					item.setId(id);
					item.setName(name);
					rooms.add(item);

				} catch (JSONException e) {
					Log.e("json", "error reading json");
					e.printStackTrace();
				}
			}
			Log.i("fetching", "fetching is complete");
			return rooms;
		}

		private void transition(String id) {

			

			
			if (instruction.equals("gateways")) {
				Bundle bundle = new Bundle();
				Fragment fragment = new ConsumerLightsFragment();

				bundle.putString("gatewayID", id);
				bundle.putString("instruction", "floors");
				fragment.setArguments(bundle);
				FragmentTransaction transaction = getActivity()
						.getSupportFragmentManager().beginTransaction();
				transaction.replace(R.id.content_frame, fragment);
				transaction.addToBackStack(null);
				transaction.commit();
			} else if (instruction.equals("floors")) {
				Bundle bundle = new Bundle();
				Fragment fragment = new ConsumerLightsFragment();
				bundle.putString("floorID", id);
				bundle.putString("gatewayID", requestedGateway);
				bundle.putString("instruction", "rooms");
				fragment.setArguments(bundle);
				FragmentTransaction transaction = getActivity()
						.getSupportFragmentManager().beginTransaction();
				transaction.replace(R.id.content_frame, fragment);
				transaction.addToBackStack(null);
				transaction.commit();
			} else if (instruction.equals("rooms")) {
				
				

				Intent intent = new Intent();
				intent = new Intent(getActivity(),
						SwitchesActivity.class);
				intent.putExtra("floor", requestedFloor);
				intent.putExtra("room", id);
				intent.putExtra("gatewayID", requestedGateway);
				startActivity(intent);
			}

		
			
		}

		@Override
		protected void onPostExecute(ArrayList<LocationItem2> rooms) {
			hideProgressBar();
			if (rooms == null) {
				AccountControl.relogin(getActivity());
			} else {
				setGatewayListView(rooms);
			}
		}

		private void setGatewayListView(ArrayList<LocationItem2> rooms) {
			
			
			if (rooms.size()==1){
				//transition to the next screen right away
				
			
					String id = (String) rooms.get(0).getId();
				
				
				transition(id);
			}else{
			
			ListView listview = (ListView) fragment.getView().findViewById(
					R.id.basic_list);
			LocationAdapter2 adapter = new LocationAdapter2(
					fragment.getActivity(), R.layout.item_one_string, rooms);

			listview.setAdapter(adapter);
			listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					LocationItem2 item = (LocationItem2) arg0.getAdapter()
							.getItem(arg2);

					
					transition( item.getId());

				}
			});
			}
		}

		private void showProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (!progressBar.isShown()) {
				progressBar.setVisibility(View.VISIBLE);
			}
		}

		private void hideProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (progressBar.isShown()) {
				progressBar.setVisibility(View.GONE);
			}
		}

	}
}