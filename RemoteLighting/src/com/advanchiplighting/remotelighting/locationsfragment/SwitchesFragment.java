package com.advanchiplighting.remotelighting.locationsfragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.ProgressBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.advanchiplighting.remotelighting.GlobalConstants;
import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.account.AccountControl;
import com.advanchiplighting.remotelighting.customlist.SwitchAdapter;
import com.advanchiplighting.remotelighting.customlist.SwitchItem;

public class SwitchesFragment extends Fragment {

	String requestedFloor;
	String requestedGateway;
	String requestedRoom;
	String requestedLightId;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.plain_basic_list, container,
				false);
		// Log.i("info from the gateway",savedInstanceState.getString("gatewayID"));
		Bundle bundle = this.getArguments();
		requestedFloor = bundle.getString("floor");
		requestedGateway = bundle.getString("gatewayID");
		requestedRoom = bundle.getString("room");
		Log.i("requested floor", requestedFloor);
		// change title
		getActivity().setTitle(requestedRoom);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		new FetchGatewaysTask().execute();
	}

	private class FetchGatewaysTask extends AsyncTask<Void, Void, ArrayList<SwitchItem>> {
		private SwitchesFragment fragment = SwitchesFragment.this;
		Map<String, String> myMap = new HashMap<String, String>();

		

		@Override
		protected void onPreExecute() {
			showProgressBar();
		}

		@Override
		protected ArrayList<SwitchItem> doInBackground(Void... params) {
			String request = "";

			Log.i("Firebase request: ", request);
			String requestAddress = request;

			StringBuilder url = new StringBuilder();
			url.append(requestAddress);
			HttpGet get = new HttpGet(url.toString());
			HttpClient client = new DefaultHttpClient();
			HttpUriRequest req = get;
			HttpResponse resp = null;
			try {
				resp = client.execute(req);
			} catch (Exception e) {
				e.printStackTrace();
			}

			HttpEntity httpEntity = resp.getEntity();

			String output = null;
			try {
				output = EntityUtils.toString(httpEntity);
				Log.i("response", output);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Log.i("fetching", "started reading the json");

			JSONObject json = null;
			try {
				json = new JSONObject(output);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			JSONArray jsonArray = null;
			try {
				jsonArray = json.getJSONArray("switches");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String name = null;
			String id = null;
			int index = 0;
			
			ArrayList<SwitchItem> switches = new ArrayList<SwitchItem>();
			for (int i = 0; i < jsonArray.length(); i++) {

				String floor = null;
				String room = null;
				String state = null;
				boolean stateBool = false;
				try {
					floor = (String) jsonArray.getJSONObject(i).get("floor");
					room = (String) jsonArray.getJSONObject(i).get("room");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (floor.equals(requestedFloor) && room.equals(requestedRoom)) {
					try {
						name = (String) jsonArray.getJSONObject(i).get("name");
						id = (String) jsonArray.getJSONObject(i).get(
								"switch_id");
						stateBool = (Boolean) jsonArray.getJSONObject(i).get(
								"state");

						switches.add(new SwitchItem(id, requestedGateway, name,
								stateBool));

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			Log.i("fetching", "fetching is complete");
			return switches;
		}

		/*
		 * private int numberOfRooms(JSONArray jsonArray, String
		 * requestedFloor,String requestedRoom) { int count = 0; for (int i = 0;
		 * i < jsonArray.length(); i++) { String room = null; String floor =
		 * null; try { floor = (String) jsonArray.getJSONObject(i).get("floor");
		 * room = (String) jsonArray.getJSONObject(i).get("room"); } catch
		 * (JSONException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * if (floor.equals(requestedFloor) && room.equals(requestedRoom)) {
		 * count++; } }
		 * 
		 * return count; }
		 */

		@Override
		protected void onPostExecute( ArrayList<SwitchItem> switches) {
			hideProgressBar();
			if (switches==null){
				AccountControl.relogin(getActivity());
			}else{
			setGatewayListView(switches);
			}
		}

		private void setGatewayListView(ArrayList<SwitchItem> switches) {
			ListView listview = (ListView) fragment.getView().findViewById(
					R.id.basic_list);
			SwitchAdapter adapter = new SwitchAdapter(getActivity(),
					R.layout.item_switch, switches);

			listview.setAdapter(adapter);
		}

		private void showProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (!progressBar.isShown()) {
				progressBar.setVisibility(View.VISIBLE);
			}
		}

		private void hideProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (progressBar.isShown()) {
				progressBar.setVisibility(View.GONE);
			}
		}

	}

}
