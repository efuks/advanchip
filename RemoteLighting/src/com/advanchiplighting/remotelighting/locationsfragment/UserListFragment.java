package com.advanchiplighting.remotelighting.locationsfragment;

import https.HTTPGetData;

import java.util.ArrayList;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.ProgressBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.advanchiplighting.remotelighting.R;
import com.advanchiplighting.remotelighting.account.AccountControl;
import com.advanchiplighting.remotelighting.customlist.ManageUserAdapter;
import com.advanchiplighting.remotelighting.customlist.ManageUserItem;
import com.advanchiplighting.remotelighting.newrequests.NewUserRequest;

public class UserListFragment extends Fragment {
	

	private static final Integer USER_ITEM = 0;
	private static final Integer ADD_USER_ITEM = 1;
	private static String requestedGateway;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.list_one_button, container, false);

		Button btn = (Button) view.findViewById(R.id.one_button);
		btn.setText("Add User");
		btn.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Dialog addUser = new Dialog(getActivity());
				addUser.setContentView(R.layout.dialog_add_user);
				addUser.setTitle("Add new user Email");

				final EditText userEmail = (EditText) addUser
						.findViewById(R.id.dialog_add_user_email);

				Button ok = (Button) addUser
						.findViewById(R.id.dialog_add_user_ok);
				Button cancel = (Button) addUser
						.findViewById(R.id.dialog_add_user_cancel);

				ok.setOnClickListener(new Button.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						String email = userEmail.getText().toString();

						JSONObject request = new JSONObject();
						try {
							request.put("email", email);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FetchUsersTaask task = new FetchUsersTaask("newUser",
								request);
						task.execute();
						addUser.dismiss();

					}
				});

				cancel.setOnClickListener(new Button.OnClickListener() {

					@Override
					public void onClick(View v) {

						addUser.dismiss();

					}
				});
				addUser.show();
			}
		});

		getActivity().setTitle("users");
		Bundle bundle = this.getArguments();
		requestedGateway = bundle.getString("gatewayID");

		FetchUsersTaask task = new FetchUsersTaask("gateways", null);
		task.execute();

		return view;

	}

	private class FetchUsersTaask extends AsyncTask<Void, Void, Object> {
		private UserListFragment fragment = UserListFragment.this;

		String instruction;
		JSONObject request;

		public FetchUsersTaask(String instruction, JSONObject details) {
			this.instruction = instruction;
			this.request = details;

		}

		@Override
		protected void onPreExecute() {

			// showProgressBar();
		}

		@Override
		protected Object doInBackground(Void... params) {

			String output;
			JSONObject json = null;

			JSONObject requestJson = new JSONObject();

			if (instruction.equals("gateways")) {
				try {
					requestJson.put("instruction", "gateway");
					requestJson.put("gatewayID", requestedGateway);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				output = HTTPGetData.fetchGatewayData(requestJson,
						getActivity());
				
				if (output.equals("relogin")){
					return null;
				}
				try {
					json = new JSONObject(output);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				JSONArray jsonArray = null;
				try {
					jsonArray = json.getJSONArray("permissions");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Log.i("whole object json", jsonArray.toString());
				ArrayList<ManageUserItem> users = new ArrayList<ManageUserItem>();
				for (int i = 0; i < jsonArray.length(); i++) {
					try {

						ManageUserItem user = new ManageUserItem();
						user.setUserName((String) jsonArray.getJSONObject(i)
								.get("userDisplayName"));
						user.setPermission((Integer) jsonArray.getJSONObject(i)
								.get("level"));
						user.setID((String) jsonArray.getJSONObject(i).get(
								"userId"));
						user.setGatewayID(requestedGateway);
						users.add(user);
					} catch (JSONException e) {
						Log.e("json", "error reading json");
						e.printStackTrace();
					}
				}
				Log.i("fetching", "fetching is complete");
				return users;

			} else if (instruction.equals("newUser")) {

				String email = null;
				try {
					email = (String) (request.get("email"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				NewUserRequest task = new NewUserRequest(requestedGateway,
						getActivity(), email);
				JSONObject result = task.addTheNewUser();

				return result;

			}
			return null;

		}

		@Override
		protected void onPostExecute(Object object) {
			// hideProgressBar();
			if (object == null) {
				AccountControl.relogin(getActivity());
			}
			if (instruction.equals("gateways")) {

				ArrayList<ManageUserItem> users = (ArrayList<ManageUserItem>) object;
				if (users == null) {
					AccountControl.relogin(getActivity());
				} else {
					setUsersList(users);
				}
			} else if (instruction.equals("newUser")) {

				JSONObject result = (JSONObject) object;
				Boolean success = false;
				try {
					success = (Boolean) result.get("Success");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (success){
					String name = null;
					try {
						name = result.getString("name");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String dialogMesage=name+" was successfully added";
					AlertDialog dialog = new AlertDialog.Builder(getActivity())
					.setMessage(dialogMesage).setTitle("Success!")
					.setPositiveButton("Ok", null).create();
			dialog.show();
			FetchUsersTaask task = new FetchUsersTaask("gateways", null);
			task.execute();
				}else{
					String dialogMesage = null;
					try {
						
						dialogMesage = result.getString("Description");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					AlertDialog dialog = new AlertDialog.Builder(getActivity())
					.setMessage(dialogMesage).setTitle("Failed!")
					.setPositiveButton("Ok", null).create();
			dialog.show();
					
				}

			}
		}

		private void showProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (!progressBar.isShown()) {
				progressBar.setVisibility(View.VISIBLE);
			}
		}

		private void hideProgressBar() {
			ProgressBar progressBar = (ProgressBar) fragment.getView()
					.findViewById(R.id.fetch_gateways_progress);
			if (progressBar.isShown()) {
				progressBar.setVisibility(View.GONE);
			}
		}

		private void setUsersList(ArrayList<ManageUserItem> users) {
			ListView listview = (ListView) fragment.getView().findViewById(
					R.id.one_button_listview);
			ManageUserAdapter adapter = new ManageUserAdapter(getActivity(),
					R.layout.item_manage_user, users);

			listview.setAdapter(adapter);

		}

	}
	


	public interface Callbacks {
	        //Callback for when button clicked.
	        public void onButtonClicked();
	}


	
}
