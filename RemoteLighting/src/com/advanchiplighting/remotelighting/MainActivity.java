package com.advanchiplighting.remotelighting;

import java.util.ArrayList;

import https.HTTPGetData;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.ProgressBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.advanchiplighting.remotelighting.account.AccountControl;
import com.advanchiplighting.remotelighting.account.LoginActivity;
import com.advanchiplighting.remotelighting.customlist.LocationAdapter2;
import com.advanchiplighting.remotelighting.customlist.LocationItem2;
import com.advanchiplighting.remotelighting.management.ManageLightsActivity;
import com.advanchiplighting.remotelighting.management.ManageLocationActivity;
import com.advanchiplighting.remotelighting.management.ManageUserActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class MainActivity extends BaseActivity {

	static AlertDialog.Builder dialog;

	/**
	 * This activity is the Home Screen which displays 4 icons (setting, manage
	 * users, manage switches and control lights)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (userIsLoggedIn()) {
			Fragment fragment = new MainFragment();
			fragment.setArguments(savedInstanceState);
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, fragment).commit();
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	public static class MainFragment extends Fragment {
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.activity_main, container,
					false);

			ImageView btn_lights = (ImageView) rootView
					.findViewById(R.id.menu_lights);
			btn_lights.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(),
							LocationsActivity.class);
					startActivity(intent);
				}
			});

			ImageView btn_locationSettings = (ImageView) rootView
					.findViewById(R.id.menu_settings);
			btn_locationSettings.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(),
							ManageLocationActivity.class);
					startActivity(intent);
				}
			});

			ImageView btn_lightSettings = (ImageView) rootView
					.findViewById(R.id.menu_lightSettings);

			btn_lightSettings.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					new Async().execute("switches");
					displayDialog("Gateways are loading");

				}

			});

			ImageView btn_userSettings = (ImageView) rootView
					.findViewById(R.id.menu_userSettings);
			btn_userSettings.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					new Async().execute("users");

					displayDialog("Gateways are loading");
				}
			});

			return rootView;
		}

		private void displayDialog(String message) {
			dialog = new AlertDialog.Builder(getActivity());

			dialog.setIcon(R.drawable.ic_launcher);
			dialog.setTitle(message);

			dialog.show();
			

		}

		private void displayGateways(String type,
				ArrayList<LocationItem2> response) {
			final String instruction = type;
			AlertDialog.Builder builderSingle = new AlertDialog.Builder(
					getActivity());

			
			
			builderSingle.setIcon(R.drawable.ic_launcher);
			builderSingle.setTitle("Select One Name:-");
			final LocationAdapter2 adapter = new LocationAdapter2(
					getActivity(), R.layout.item_one_string, response);

			// need real data

			builderSingle.setNegativeButton("cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});

			builderSingle.setAdapter(adapter,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							final String strName = ((LocationItem2) (adapter
									.getItem(which))).getName();
							String selectedId = ((LocationItem2) (adapter
									.getItem(which))).getId();

							String instr = instruction;
							if (instr.equals("users")) {
								Intent intent = new Intent(getActivity(),
										ManageUserActivity.class);
								intent.putExtra("gatewayID", selectedId);
								startActivity(intent);
							}
							if (instr.equals("switches")) {
								Intent intent = new Intent(getActivity(),
										ManageLightsActivity.class);
								intent.putExtra("gatewayID", selectedId);
								startActivity(intent);
							}
						}
						// ---
					});
			builderSingle.show();
		}

		private class Async extends
				AsyncTask<String, String, ArrayList<LocationItem2>> {
			private MainFragment fragment = MainFragment.this;
			String instruction;

			@Override
			protected void onPreExecute() {
				Log.i("fetching", "started");

			}

			@Override
			protected ArrayList<LocationItem2> doInBackground(String... params) {
				instruction = params[0];
				String output;
				JSONObject requestJson = new JSONObject();

				try {
					requestJson.put("instruction", "gateways");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				output = HTTPGetData.fetchGatewayData(requestJson,
						getActivity());
				if (output.equals("relogin")) {
					return null;
				}
				JSONArray jsonArray = null;
				try {
					jsonArray = new JSONArray(output);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Log.i("json", jsonArray.toString());

				ArrayList<LocationItem2> gateways = new ArrayList<LocationItem2>();
				for (int i = 0; i < jsonArray.length(); i++) {
					try {

						String name = (String) jsonArray.getJSONObject(i).get(
								"name");

						String id = (String) jsonArray.getJSONObject(i).get(
								"_id");
						LocationItem2 item = new LocationItem2();
						item.setId(id);
						item.setName(name);
						gateways.add(item);

					} catch (JSONException e) {
						Log.e("json", "error reading json");
						e.printStackTrace();
					}
				}
				Log.i("fetching", "fetching is complete");
				return gateways;

			}

			@Override
			protected void onPostExecute(
					ArrayList<LocationItem2> responseFetched) {

				if (responseFetched == null) {
					Intent intent = new Intent(getActivity(),
							LoginActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				} else {
					Log.i("fetching",
							"task complete, " + responseFetched.toString());

					displayGateways(instruction, responseFetched);
				}

			}

			private void showProgressBar() {
				ProgressBar progressBar = (ProgressBar) fragment.getView()
						.findViewById(R.id.progress);
				if (!progressBar.isShown()) {
					progressBar.setVisibility(View.VISIBLE);
				}
			}

			private void hideProgressBar() {
				ProgressBar progressBar = (ProgressBar) fragment.getView()
						.findViewById(R.id.fetch_gateways_progress);
				if (progressBar.isShown()) {
					progressBar.setVisibility(View.GONE);
				}
			}

		}
	}
}
