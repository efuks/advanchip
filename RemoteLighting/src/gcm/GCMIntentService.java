/**
 * GCMIntentService.java handles receiving the push notification, waking up the 
 * device  and  providing the clickable buttton  which leads to the new activiy 
 * with more details about the details
 */
package gcm;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.advanchiplighting.remotelighting.GlobalConstants;
import com.advanchiplighting.remotelighting.R;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCM Tutorial::Service";

	// Use your PROJECT ID from Google API into SENDER_ID
	public static final String SENDER_ID = GlobalConstants.projectNumber;

	public GCMIntentService() {
		super(SENDER_ID);
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {

		Log.i(TAG, "onRegistered: registrationId=" + registrationId);
		GlobalConstants.deviceID = registrationId;
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {

		Log.i(TAG, "onUnregistered: registrationId=" + registrationId);
	}

	@Override
	protected void onMessage(Context context, Intent data) {
		String message;
		String light;
		String state;

		message = data.getStringExtra("message");
		light = data.getStringExtra("light");
		state = data.getStringExtra("state");

		// LightActivity.updateTheView(message, light, state);

		/*
		 * uncoment the following block of code to enable new activity for
		 * watching the details of the gcm
		 * 
		 * // Open a new activity called GCMMessageView Intent intent = new
		 * Intent(this, GCMMessageView.class); // Pass data to the new activity
		 * intent.putExtra("message", message); intent.putExtra("light", light);
		 * intent.putExtra("state", state); // Starts the activity on
		 * notification click
		 */
		
		
		Intent intent =null; //remive it
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		// Create the notification with a notification builder
		Notification notification = new NotificationCompat.Builder(this)
				.setSmallIcon(R.drawable.ic_launcher)
				.setWhen(System.currentTimeMillis())
				.setContentTitle("Android GCM Tutorial")
				.setContentText(message).setContentIntent(pIntent)
				.getNotification();
		// Remove the notification on click
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		manager.notify(R.string.app_name, notification);

		{
			// Wake Android Device when notification received
			PowerManager pm = (PowerManager) context
					.getSystemService(Context.POWER_SERVICE);
			final PowerManager.WakeLock mWakelock = pm.newWakeLock(
					PowerManager.FULL_WAKE_LOCK
							| PowerManager.ACQUIRE_CAUSES_WAKEUP, "GCM_PUSH");
			mWakelock.acquire();

			// Timer before putting Android Device to sleep mode.
			Timer timer = new Timer();
			TimerTask task = new TimerTask() {
				public void run() {
					mWakelock.release();
				}
			};
			timer.schedule(task, 5000);
		}

	}

	@Override
	protected void onError(Context arg0, String errorId) {

		Log.e(TAG, "onError: errorId=" + errorId);
	}

}