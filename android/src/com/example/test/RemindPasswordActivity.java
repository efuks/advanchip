package com.example.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class RemindPasswordActivity extends Activity {

	private Button backToLoginButton;
	private Button remindPasswordButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.remind_password);

		backToLoginButton = (Button) findViewById(R.id.BackToLoginButton);
		remindPasswordButton = (Button) findViewById(R.id.RemindButton);

		backToLoginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(RemindPasswordActivity.this,
						MainActivity.class);
				startActivity(intent);
			}
		});

		remindPasswordButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(RemindPasswordActivity.this,
						MainActivity.class);
				startActivity(intent);
			}
		});
	}

}
